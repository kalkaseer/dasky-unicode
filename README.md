# Dasky Unicode

UTF-8 and Unicode library. Handles conversion from and to UTF-16 and UTF-32. More tests and more functionalities will be added later.
