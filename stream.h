//
//  stream.h
//  dasky
//
//  Created by Kareem Alkaseer on 20/12/2017.
//
//

#ifndef dask_utf_stream_h__
#define dask_utf_stream_h__

#include "dasky/utf/config.h"
#include "dasky/utf/codec.h"

BEGIN_DASKY_NAMESPACE
namespace utf {
  
namespace details {
  
template< class CharT >
struct DASKY_LOCAL null_streambuf_action {
  declare_always_inline
  void operator()(rune cp, CharT * current, CharT * end) const {}
};
  
template< class CharT >
struct DASKY_LOCAL unsafe_streambuf_action {
  declare_always_inline
  void operator()(rune cp, CharT * current, CharT * end) const {
    if (! utf::is_safe(cp)) {
      auto rsize = byte_size_from_lead(*current);
      *current = ' '; ++current;
      if (rsize > 1) {
        memmove(current, (current + rsize - 1), (end - current + rsize - 1));
        end -= rsize - 1;
      }
    }
  }
};
} /* namespace details */
  
template<
  class CharT, class CharTraits = std::char_traits<CharT>,
  class Action = details::null_streambuf_action<CharT> >
class DASKY_API basic_streambuf
  : public std::basic_streambuf<CharT, CharTraits>
{
public:
  
  using source_type = std::basic_streambuf<CharT, CharTraits>;
  using typename source_type::int_type;
  using typename source_type::char_type;
  using typename source_type::traits_type;
  
  explicit basic_streambuf(source_type & src)
    : m_source{ src }
  {
    // initially the buffer needs underflow() to get more characters
    // from the underlying buffer.
    this->setg(m_buffer,
               m_buffer + buffer_size,
               m_buffer + buffer_size);
  }
  
  basic_streambuf(const basic_streambuf & rhs)
    : std::basic_streambuf<source_type>(rhs)
  {
    memcpy(m_buffer, rhs.m_buffer, buffer_size);
  }
  
  basic_streambuf(basic_streambuf && rhs)
    : std::basic_streambuf<source_type>(rhs)
  {
    memmove(m_buffer, rhs.m_buffer, buffer_size);
  }
  
  basic_streambuf & operator=(const basic_streambuf & rhs) {
    if (this != &rhs) {
      source_type::operator=(rhs);
      memcpy(m_buffer, rhs.m_buffer, buffer_size);
    }
    return *this;
  }
  
  basic_streambuf & operator=(basic_streambuf && rhs) {
    if (this != &rhs) {
      source_type::operator=(rhs);
      memmove(m_buffer, rhs.m_buffer, buffer_size);
    }
    return *this;
  }
  
  virtual ~basic_streambuf() {}
  
protected:
  
  /*
   * * * * * * * * | * *
   b               g
                   e
   0 1 1 1 2 2 3 3 | 3 3
   b
   g
   ...
   0 1 1 1 2 2 3 3 | 3 3
                   b
               g
               e
   3 3 * * * * * * | 3 3
       g       e
   3 3 3 3 * * * * |
   b       e
   g
   3 3 3 3 * * * * |
     g     e
   3 3 3 3 * * * * |
       g   e
   3 3 3 3 * * * * |
         g e
   #
   
   
   * * * * * * * * | * *
   b               g
                   e
   0 1 1 1 @ 2 3 3 | 3 3
   g               e
   ...
   0 1 1 1 @ 2 3 3 | 3 3
           g       e
   0 1 1 1 2 3 3 * | 3 3
           g     e | 3 3
   0 1 1 1 3 3 * * | 3 3
           g   e   | 3 3
   0 1 1 1 3 3 * * | 3 3
           g   b
           e
   3 3 * * * * * * | 3 3
       g
   3 3 3 3 * * * * |
   g       e
           g
   b       e
   g
   
   */

  virtual int_type underflow() override {
    
    if (this->gptr() < this->egptr())
      return traits_type::to_int_type(*this->gptr());
    
    int char_count;
    rune cp;
    int nbytes;
    char_type * g;
    
    if (this->egptr() > this->eback()) {
      char_count = buffer_size;
      g = m_buffer;
    } else {
      int offset = this->eback() - this->egptr();
      memmove(m_buffer, this->egptr(), offset);
      char_count = buffer_size - offset;
      g = m_buffer + offset;
    }
    
    std::streamsize n = m_source.xsgetn(g, char_count);
    if (n == 0 )
      return traits_type::eof();
    
    char_type * e = g + n;
    g = m_buffer;
    
    while (g < e) {
    
      nbytes = utf::next(g, e, cp);
      if (nbytes < 0) {
        
        g += nbytes;
        auto rsize = byte_size_from_lead(g);
        if ((e - g) < rsize) { // valid lead byte but buffer
                               // does not contain enough bytes,
                               // we know this as e-g can never be < 0
          this->setg(e, g, g);
          break;
        }
        // invalid sequence, skip to the next byte
//        memmove(g, g+1, e-g-1);
//        --e;
        *g = ' '; ++g;
        
        continue; // do not execute action if invalid
      }
      
      m_action(cp, g, e);
    }
    
    this->setg(m_buffer, m_buffer, e);
  }
  
protected:
  
  static constexpr int buffer_size{ 8 };
  
  char_type     m_buffer[buffer_size];
  source_type & m_source;
  Action        m_action;
};
  
using streambuf    = basic_streambuf<char>;
using u16streambuf = basic_streambuf<ch16>;
using u32streambuf = basic_streambuf<ch32>;
using wstreambuf   = basic_streambuf<wchar_t>;
  
template< class CharT, class CharTraits = std::char_traits<CharT> >
class DASKY_API basic_safe_streambuf
  : public basic_streambuf<
      CharT, CharTraits, details::unsafe_streambuf_action<CharT>>
{
public:
  using basic_streambuf<
    CharT,
    CharTraits,
    details::unsafe_streambuf_action<CharT>
  >::basic_streambuf;
};
  
using safe_streambuf    = basic_safe_streambuf<char>;
using safe_u16streambuf = basic_safe_streambuf<ch16>;
using safe_u32streambuf = basic_safe_streambuf<ch32>;
using safe_wstreambuf   = basic_safe_streambuf<wchar_t>;
  
class istream {
  // TODO
};
  
class ostream {
  // TODO
};
  
class ifstream {
  // TODO
};
  
class ofstream {
  // TODO
};
  
} /* namespace dasky */
END_DASKY_NAMESPACE


#endif /* dask_utf_stream_h__ */
