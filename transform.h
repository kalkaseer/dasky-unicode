//
//  transform.h
//  dasky
//
//  Created by Kareem Alkaseer on 20/12/2017.
//
//

#ifndef dasky_utf_transform_h__
#define dasky_utf_transform_h__

#include "dasky/utf/config.h"
#include "dasky/utf/codec.h"

BEGIN_DASKY_NAMESPACE
namespace utf {
  
/// Ensure that a UTF-8 byte sequence contains only web-safe codepoints.
/// Any unsafe codepoint is replace with a whitespace. The buffer is modified
/// *in-place@ if it needs modification.
/// No memory allocations or invalid access may occur as
/// any modification will result in either the same number of bytes or fewer.
///
/// `ByteIt` is an itertor over a coninguous owned memory range resembling
/// raw pointers.
///
/// @param begin Sequence start.
/// @param end Sequence end.
/// @tparam ByteIt A byte iterator modeling the description mentioned above.
/// @return Past-the-last pointer to signal the end of the modified
/// buffer or `end` if the buffer was not modified.
template< class ByteIt >
DASKY_API
ByteIt to_safe(ByteIt begin, ByteIt end) {
  
  utf::rune cp;
  int nbytes;
  
  // Return if buffer is empty.
  if (begin == end) return end;
  
  while (true) {
    
    utf::seek_unsafe(begin, end);
    if (begin != end) {
      
      auto it{ begin };
      nbytes = utf::next(it, end, cp);
      
      // In both cases, invalid or unsafe, we will advance to the next byte
      // so we start searching from the next lead byte.
      *begin = ' ';
      ++begin;
      
      // nbytes > 0:
      //   valid but unsafe: shift buffer so that the next codepoint's lead
      //   byte is right next to begin
      // nbyte < 0:
      //   invalid
      if (nbytes > 1) {
        std::memmove(begin, it, end - it);
        end -= nbytes - 1;  // we replaced the first byte already
      } else {
        end -= -nbytes - 1; // we replaced the first byte already
      }
      
      continue;
    }
    
    return end;
  }
}
  
/// Ensure that a UTF-8 byte sequence contains only web-safe codepoints.
/// Any unsafe codepoint is replace with a whitespace. The buffer is modified
/// *in-place* if it needs modification. The sequence is supposed to be
/// contained in a container whose characteristics are as follows:
///   - has an STL-compliant begin() returning an `InputIterator`
///   - has an STL-compliant end() returning and `InputIterator`
///   - has an STL-compliant empty()
///   - has a member type `iterator`
///   - has member function `erase(iterator begin, iterator end)`
///   - erasing at position does not invalidate iterators to positions before
///     that position but may or may not invalidate iterators to positions
///     after.
/// The above characters are valid for orderderd STL containers, including
/// `std::string`.
///
/// @param begin Sequence start.
/// @param end Sequence end.
/// @tparam Container Models a container whose characteristics are mentioned
///         above
/// @return Past-the-last pointer to signal the end of the modified
///         buffer or `end` if the buffer was not modified.
template< class Container >
DASKY_API
typename Container::iterator to_safe(Container & data) {
  
  utf::rune cp;
  int nbytes;
  
  if (data.empty()) return data.end();
  
  typename Container::iterator begin{ data.begin() };
  typename Container::iterator end{ data.end() };
  typename Container::iterator it;
  
  while (true) {
    
//    end = data.end();
    utf::seek_unsafe(begin, end);
    if (begin != end) {
      
      it = begin;
      nbytes = utf::next(it, end, cp);
      // In both cases, invalid or unsafe, we will advance to the next byte
      // so we start searching from the next lead byte.
      *begin = ' ';
      ++begin; // we never expand the data, begin will not be invalidated.
      if (nbytes > 1) {
        // valid but unsafe:
        // shift buffer so that the next codepoint lead byte is right next
        // to begin
//        begin = data.erase(begin, it);
        begin = std::move(it, end, begin);
        end -= it - begin;
      }
      
      continue;
    }
    data.erase(begin, data.erase());
    return end;
  }
}
  

  
template< class InputIt >
struct DASKY_LOCAL scan_unsafe {
  declare_always_inline
  void operator()(InputIt & first, InputIt & last) const {
    utf::seek_unsafe(first, last);
  }
};
  
template< class InputIt >
struct DASKY_LOCAL advance_unsafe {
  
  declare_always_inline void operator()(
    InputIt & first, InputIt & last, rune & cp, int nbytes
  ) const {
    
    nbytes = utf::next(first, last, cp);
    
    // begin is pointing to the first unconsumed byte.
    // nbytes > 0: carry on scanning from that byte, unsafe rune was skipped.
    // nbyte < 0: rewind begin to point to the next byte to the one that
    //            started the invalid sequence so we may find valid a
    //            sequence starting from the next byte.
    
    if (nbytes < 0) {
      // advance the iterator to the next byte
      advance_iterator(first, -nbytes-1);
    }
  }
};
  
template< class InputIt >
struct DASKY_LOCAL scan_invalid {
  opt_always_inline void operator()(InputIt & first, InputIt & last) const {
    utf::seek_invalid(first, last);
  }
};
  
template< class InputIt >
struct DASKY_LOCAL advance_invalid {
  
  declare_always_inline void operator()(
    InputIt & first, InputIt & last, rune & cp, int & nbytes
  ) const {
    ++first;
  }
};
  
/// Ensure that a UTF-8 byte sequence contains only codepoints filtered by
/// a predicate.
/// If a codepoint not accepted by the predicate is found, it is replaced by
/// a whitespace and the modified sequence is copied into `out`.
/// If the sequence is all valid, it is not copied and the returned iterator
/// will be equal to `out`.
///
/// `Scan` is a callable `void(InputIt& begin, InputIt& end)` that points
/// `begin` to the first encountered unwanted codepoint's lead byte.
///
/// @param begin Input sequence start.
/// @param end Input seqeuence end.
/// @param out Output sequence start, whose capacity must be equal to at least
///            `end - begin - 3` in case only one 4-byte unwanted codepoint
///            is in the sequence which gets replaced by a 1-byte codepoint.
/// @param scan Scanning predicte.
/// @tparam InputIt Byte input iterator.
/// @tparam OutputIt Byte output iterator.
/// @tparam Scan Predicate that models the concept mentioned above.
/// @return Output iterator pointing to the last written byte into the output
///                range. This will be equal to `out` if nothing was written.
template< class InputIt, class OutputIt, class Scan, class Advance >
DASKY_API
OutputIt transform(
  InputIt begin, InputIt end, OutputIt out, Scan scan, Advance advance
) {
  
  utf::rune cp;
  int nbytes;
  bool marked_found{ false }; // previously found a marked sequence.
  auto start{ begin };        // used to copy safe sub-ranges.
  
  // Return if buffer is empty.
  if (begin == end) return out;
  
  while (true) {
    
    scan(begin, end);
    if (begin != end) {
      
      if (marked_found) {
        out = std::copy(start, begin, out);
        start = begin;
      }
      
      *out = ' '; ++out;
      advance(begin, end, cp, nbytes);
      start = begin;
      
      marked_found = true;
      continue;
    }
    
    if (marked_found)
      std::copy(start, end, out);
    
    return out;
  }
}
  
/// Ensure that a UTF-8 byte sequence contains only web-safe codepoints.
/// If an unsafe codepoint is found, it is replaced by a whitespace and the
/// modified sequence is copied into `out`.
/// If the sequence is safe, it is not copied.
///
/// @param begin Input sequence start.
/// @param end Input seqeuence end.
/// @param out Output sequence start, whose capacity must be equal to at least
///            `end - begin - 3` in case only one 4-byte unsafe codepoint
///            is in the sequence which gets replaced by a 1-byte codepoint.
/// @tparam InputIt Byte input iterator.
/// @tparam OutputIt Byte output iterator.
/// @return Output iterator pointing to the last written byte into the output
///                range. This will be equal to `out` if nothing was written.
template< class InputIt, class OutputIt >
declare_always_inline DASKY_API
OutputIt to_safe(InputIt begin, InputIt end, OutputIt out) {
  return transform(
    begin, end, out, scan_unsafe<InputIt>(), advance_unsafe<InputIt>());
}
  
template< class ByteIt >
DASKY_API
ByteIt to_valid(ByteIt begin, ByteIt end) {
  
  // Return if buffer is empty.
  if (begin == end) return end;
  
  while (true) {
    
    utf::seek_invalid(begin, end);
    if (begin != end) {
      *begin = ' ';
      ++begin;
      continue;
    }
    
    return end;
  }
}
  
template< class Container >
DASKY_API
typename Container::iterator to_valid(Container & data) {
  
  if (data.empty()) return data.end();
  
  typename Container::iterator begin{ data.begin() };
  typename Container::iterator end{ data.end() };
  
  while (true) {
    
    utf::seek_invalid(begin, end);
    if (begin != end) {
      // Advance to the next byte so we start searching from the next lead byte.
      *begin = ' ';
      ++begin;
      continue;
    }
    
    return end;
  }
}
  
template< class InputIt, class OutputIt >
declare_always_inline DASKY_API
OutputIt to_valid(InputIt begin, InputIt end, OutputIt out) {
  return transform(
    begin, end, out, scan_invalid<InputIt>(), advance_invalid<InputIt>());
}
  
} /* namespace utf */
END_DASKY_NAMESPACE

#endif /* dasky_utf_transform_h__ */
