//
//  conversion.h
//  dasky
//
//  Created by Kareem Alkaseer on 08/12/2017.
//
//

#ifndef dasky_utf_conversion_h
#define dasky_utf_conversion_h

#include "dasky/utf/config.h"
#include "dasky/utf/codec.h"

BEGIN_DASKY_NAMESPACE
namespace utf {
  
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter, class OutputCh16Iter >
OutputCh16Iter to_utf16(ByteIter first, ByteIter last, OutputCh16Iter out) {
  
  using namespace details;
  int nbytes;
  rune cp;
  while (first != last) {
    nbytes = next<replacement, ErrorPolicy>(first, last, cp);
    if (nbytes > 0) {
      if (cp <= 0xffff) {
        *out = static_cast<ch16>(cp);
        ++out;
      } else { // encode to a surrogate
        *out = static_cast<ch16>((cp >> 10) + lead_offset);
        ++out;
        *out = static_cast<ch16>((cp & 0x3ff) + trail_surrogate_min);
        ++out;
      }
    } else {
      details::decoding_error_policy<ErrorPolicy>::execute(cp);
      first += -nbytes - 1;
    }
  }
  return out;
}
  
template<
  class ErrorPolicy = default_error_policy_tag,
  class Ch16Iter,
  class OutputByteIt >
OutputByteIt from_utf16(Ch16Iter first, Ch16Iter last, OutputByteIt out) {
  
  rune cp;
  rune surrogate_end;
  while (first != last) {
    cp = mask16<rune, rune>(*first);
    if (details::is_lead_surrogate(cp)) {
      ++first;
      if (first == last) {
        details::decoding_error_policy<ErrorPolicy>::execute(cp);
        continue;
      }
      surrogate_end = mask16<rune, rune>(*first);
      if (!details::is_trail_surrogate(surrogate_end)) {
        details::decoding_error_policy<ErrorPolicy>::execute(cp);
        continue;
      }
      cp = (cp << 10) + surrogate_end + details::surrogate_offset;
    }
    to_bytes(cp, out);
    ++first;
  }
  return out;
}
  
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter,
  class OutputCh32Iter >
OutputCh32Iter to_utf32(ByteIter first, ByteIter last, OutputCh32Iter out) {
  
  rune cp;
  int nbytes;
  while (first != last) {
    nbytes = next(first, last, cp);
    if (nbytes > 0) {
      *out = cp;
      ++out;
    } else {
      details::decoding_error_policy<ErrorPolicy>::execute();
      first += -nbytes - 1;
    }
  }
  return out;
}
  
template<
  class Validator = replacing_validator<>,
  class Ch32Iter,
  class OutputByteIt >
OutputByteIt from_utf32(Ch32Iter first, Ch32Iter last, OutputByteIt out) {
  
  for (; first != last; ++first)
    to_bytes(Validator::validate(*first), out);
  return out;
}
  
namespace unchecked {
  
template< class ByteIter, class OutputCh16Iter >
OutputCh16Iter to_utf16(ByteIter first, ByteIter last, OutputCh16Iter out) {
  
  using namespace details;
  rune cp;
  while (first != last) {
    cp = next(first);
    if (cp <= 0xffff) {
      *out = static_cast<ch16>(cp);
      ++out;
    } else { // encode to a surrogate
      *out = static_cast<ch16>((cp >> 10) + lead_offset);
      ++out;
      *out = static_cast<ch16>((cp & 0x3ff) + trail_surrogate_min);
      ++out;
    }
  }
  return out;
}
  
template< class Ch16Iter, class OutputByteIt >
OutputByteIt from_utf16(Ch16Iter first, Ch16Iter last, OutputByteIt out) {
  
  rune cp;
  rune surrogate_end;
  while (first != last) {
    cp = mask16<rune, rune>(*first);
    if (details::is_lead_surrogate(cp)) {
      ++first;
      surrogate_end = mask16<rune, rune>(*first);
      cp = (cp << 10) + surrogate_end + details::surrogate_offset;
    }
    to_bytes(cp, out);
    ++first;
  }
  return out;
}
  
template< class ByteIter, class OutputCh32Iter >
OutputCh32Iter to_utf32(ByteIter first, ByteIter last, OutputCh32Iter out) {
  
  while (first != last) {
    *out = unchecked::next(first);
    ++out;
  }
  return out;
}
  
template< class Ch32Iter, class OutputByteIt >
OutputByteIt from_utf32(Ch32Iter first, Ch32Iter last, OutputByteIt out) {
  
  for (; first != last; ++first)
    to_bytes(*first, out);
  return out;
}
  
} /* namespace unchecked */
  
} /* namespace utf */
END_DASKY_NAMESPACE

#endif /* dasky_utf_conversion_h */
