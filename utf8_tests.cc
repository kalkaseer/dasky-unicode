//
//  utf8_tests.cc
//  dasky
//
//  Created by Kareem Alkaseer on 04/12/2017.
//
//

#include <string>
#include <array>
#include <vector>
#include <iterator>
#include <bitset>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "dasky/utf/utf.h"

using namespace ::testing;

#if !defined(DASKY_OUT_OF_TREE_BUILD)
using namespace dasky;
using namespace dasky::utf;
#else
using namespace utf;
#endif

namespace dasky {
  
class utf_tests : public ::testing::Test {
protected:
  
  void decode_encode(
    const std::string   & str,
    utf::ustring        & ustr, // decode to UTF8 into this
    std::string         & dstr  // encode to bytes into this
  ) {
    utf::decode(str.begin(), str.end(), std::back_inserter(ustr));
    utf::encode(ustr.begin(), ustr.end(), std::back_inserter(dstr));
  }
  
  std::pair< std::size_t, std::size_t > decode_encode_buffer(
    const std::string & str,
    utf::rune         * ustr, // decode to UTF8 into this
    std::size_t        usize,
    char              * dstr,  // encode to bytes into this
    std::size_t        dsize
  ) {
    auto * dout = utf::decode(str.data(), str.size(), ustr);
    auto decoded_size = static_cast<std::size_t>(dout - ustr);
    auto * eout = utf::encode(ustr, decoded_size, dstr);
    auto encoded_size = static_cast<std::size_t>(eout - dstr);
    return { decoded_size, encoded_size };
  }
  
  void SetUp() {
    
    english_bytes = 
    "Hello there (2) Hello there (3) Hello there (4) Hello there (5) Hello "
    "there (6) Hello there (7) Hello there (7) Hello there (8) Hello "
    "there!!!";
    
    russian_bytes =
      u8"Independent рассказала британским фанатам об особенностях "
      u8"российского мундиаля (2) Independent рассказала британским "
      u8"фанатам об особенностях российского мундиаля";
    
    arabic_bytes = u8"اه بتاع جنان اه بتاع جنان اه بتاع جنان اه بتاع جنان";
    
    chinese_bytes =
      u8"久有归天愿终过鬼门关千里来寻归宿春华变苍颜到处群魔乱舞更有妖雾盘绕暗道入阴间过了"
      u8"阎王殿险处不须看风雷动旌旗奋忆人寰八十三年过去弹指一挥间中原千军逐蒋城楼万众检阅"
      u8"褒贬满载还世上无难事只怕我癫痫";
    
    invalid_bytes = u8"\xff\x4";
    
    errorstring = std::string{ utf::error_chars, utf::error_bytes_size };
    errorstring.append(1, 0x4);
    error_unicode = { utf::error_rune, 0x4 };
    error_byte_size = 4;
    
    english_unicode = {
      0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x74, 0x68, 0x65, 0x72,
      0x65, 0x20, 0x28, 0x32, 0x29, 0x20, 0x48, 0x65, 0x6c, 0x6c,
      0x6f, 0x20, 0x74, 0x68, 0x65, 0x72, 0x65, 0x20, 0x28, 0x33,
      0x29, 0x20, 0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x74, 0x68,
      0x65, 0x72, 0x65, 0x20, 0x28, 0x34, 0x29, 0x20, 0x48, 0x65,
      0x6c, 0x6c, 0x6f, 0x20, 0x74, 0x68, 0x65, 0x72, 0x65, 0x20,
      0x28, 0x35, 0x29, 0x20, 0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20,
      0x74, 0x68, 0x65, 0x72, 0x65, 0x20, 0x28, 0x36, 0x29, 0x20,
      0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x74, 0x68, 0x65, 0x72,
      0x65, 0x20, 0x28, 0x37, 0x29, 0x20, 0x48, 0x65, 0x6c, 0x6c,
      0x6f, 0x20, 0x74, 0x68, 0x65, 0x72, 0x65, 0x20, 0x28, 0x37,
      0x29, 0x20, 0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x74, 0x68,
      0x65, 0x72, 0x65, 0x20, 0x28, 0x38, 0x29, 0x20, 0x48, 0x65,
      0x6c, 0x6c, 0x6f, 0x20, 0x74, 0x68, 0x65, 0x72, 0x65, 0x21,
      0x21, 0x21
    };
    
    russian_unicode = {
      0x49,  0x6e,  0x64,  0x65,  0x70,  0x65,  0x6e,  0x64,  0x65,
      0x6e,  0x74,  0x20,  0x440, 0x430, 0x441, 0x441, 0x43a, 0x430,
      0x437, 0x430, 0x43b, 0x430, 0x20,  0x431, 0x440, 0x438, 0x442,
      0x430, 0x43d, 0x441, 0x43a, 0x438, 0x43c, 0x20,  0x444, 0x430,
      0x43d, 0x430, 0x442, 0x430, 0x43c, 0x20,  0x43e, 0x431, 0x20,
      0x43e, 0x441, 0x43e, 0x431, 0x435, 0x43d, 0x43d, 0x43e, 0x441,
      0x442, 0x44f, 0x445, 0x20,  0x440, 0x43e, 0x441, 0x441, 0x438,
      0x439, 0x441, 0x43a, 0x43e, 0x433, 0x43e, 0x20,  0x43c, 0x443,
      0x43d, 0x434, 0x438, 0x430, 0x43b, 0x44f, 0x20,  0x28,  0x32,
      0x29,  0x20,  0x49,  0x6e,  0x64,  0x65,  0x70,  0x65,  0x6e,
      0x64,  0x65,  0x6e,  0x74,  0x20,  0x440, 0x430, 0x441, 0x441,
      0x43a, 0x430, 0x437, 0x430, 0x43b, 0x430, 0x20,  0x431, 0x440,
      0x438, 0x442, 0x430, 0x43d, 0x441, 0x43a, 0x438, 0x43c, 0x20,
      0x444, 0x430, 0x43d, 0x430, 0x442, 0x430, 0x43c, 0x20,  0x43e,
      0x431, 0x20,  0x43e, 0x441, 0x43e, 0x431, 0x435, 0x43d, 0x43d,
      0x43e, 0x441, 0x442, 0x44f, 0x445, 0x20,  0x440, 0x43e, 0x441,
      0x441, 0x438, 0x439, 0x441, 0x43a, 0x43e, 0x433, 0x43e, 0x20,
      0x43c, 0x443, 0x43d, 0x434, 0x438, 0x430, 0x43b, 0x44f
    };
    
    arabic_unicode = {
      0x627, 0x647, 0x20,  0x628, 0x62a, 0x627, 0x639, 0x20,  0x62c,
      0x646, 0x627, 0x646, 0x20,  0x627, 0x647, 0x20,  0x628, 0x62a,
      0x627, 0x639, 0x20,  0x62c, 0x646, 0x627, 0x646, 0x20,  0x627,
      0x647, 0x20,  0x628, 0x62a, 0x627, 0x639, 0x20,  0x62c, 0x646,
      0x627, 0x646, 0x20,  0x627, 0x647, 0x20,  0x628, 0x62a, 0x627,
      0x639, 0x20,  0x62c, 0x646, 0x627, 0x646
    };
    
    chinese_unicode = {
      0x4e45, 0x6709, 0x5f52, 0x5929, 0x613f, 0x7ec8, 0x8fc7, 0x9b3c,
      0x95e8, 0x5173, 0x5343, 0x91cc, 0x6765, 0x5bfb, 0x5f52, 0x5bbf,
      0x6625, 0x534e, 0x53d8, 0x82cd, 0x989c, 0x5230, 0x5904, 0x7fa4,
      0x9b54, 0x4e71, 0x821e, 0x66f4, 0x6709, 0x5996, 0x96fe, 0x76d8,
      0x7ed5, 0x6697, 0x9053, 0x5165, 0x9634, 0x95f4, 0x8fc7, 0x4e86,
      0x960e, 0x738b, 0x6bbf, 0x9669, 0x5904, 0x4e0d, 0x987b, 0x770b,
      0x98ce, 0x96f7, 0x52a8, 0x65cc, 0x65d7, 0x594b, 0x5fc6, 0x4eba,
      0x5bf0, 0x516b, 0x5341, 0x4e09, 0x5e74, 0x8fc7, 0x53bb, 0x5f39,
      0x6307, 0x4e00, 0x6325, 0x95f4, 0x4e2d, 0x539f, 0x5343, 0x519b,
      0x9010, 0x848b, 0x57ce, 0x697c, 0x4e07, 0x4f17, 0x68c0, 0x9605,
      0x8912, 0x8d2c, 0x6ee1, 0x8f7d, 0x8fd8, 0x4e16, 0x4e0a, 0x65e0,
      0x96be, 0x4e8b, 0x53ea, 0x6015, 0x6211, 0x766b, 0x75eb
    };
  }
  
  std::string english_bytes;
  std::string russian_bytes;
  std::string arabic_bytes;
  std::string chinese_bytes;
  std::string invalid_bytes;
  std::string errorstring;
  
  utf::ustring english_unicode;
  utf::ustring russian_unicode;
  utf::ustring arabic_unicode;
  utf::ustring chinese_unicode;
  utf::ustring error_unicode;
  
  size_t error_byte_size;
};
  
TEST_F(utf_tests, TestIsComplete) {
  
  std::vector<u8> str{ u8('a') };
  EXPECT_TRUE(utf::is_complete(str.begin(), str.end()));
  
  str = { u8('\xd0'), u8('\xb1')  }; // б
  EXPECT_TRUE(utf::is_complete(str.begin(), str.end()));
}
  
TEST_F(utf_tests, TestIsCompleteBuffer) {
  
  std::string str{ 'a', 'b' };
  EXPECT_TRUE(utf::is_complete(str.data(), 1));
  
  str = { char('\xd0'), char('\xb1'), char('b')  }; // б
  EXPECT_TRUE(utf::is_complete(str.data(), 2));
}
  
TEST_F(utf_tests, TestIsValidCodepoint) {
  
  std::vector<u8> invalid{ u8(0xff) };
  EXPECT_FALSE(utf::is_valid_rune(invalid.begin(), invalid.end()));
  
  std::vector<u8> str{ u8('a') };
  EXPECT_TRUE(utf::is_valid_rune(str.begin(), str.end()));
  
  str = { u8('\xd0'), u8('\xb1')  }; // б
  EXPECT_TRUE(utf::is_valid_rune(str.begin(), str.end()));
}
  
TEST_F(utf_tests, TestIsValidCodepointBuffer) {
  
  std::string invalid{ char(0xff) };
  EXPECT_FALSE(utf::is_valid_rune(invalid.data()));
  
  std::string str{ 'a' };
  EXPECT_TRUE(utf::is_valid_rune(str.data()));
  
  str = { char('\xd0'), char('\xb1')  }; // б
  EXPECT_TRUE(utf::is_valid_rune(str.data()));
}
  
TEST_F(utf_tests, TestCodepointSize) {
  
  size_t rsize = utf::runes_size(russian_bytes.begin(), russian_bytes.end());
  EXPECT_EQ(russian_unicode.size(), rsize)
    << "Russian string codepoint size mismatch";
  
  rsize = utf::runes_size(english_bytes.begin(), english_bytes.end());
  EXPECT_EQ(english_unicode.size(), rsize)
    << "English string codepoint size mismatch";
  
  rsize = utf::runes_size(arabic_bytes.begin(), arabic_bytes.end());
  EXPECT_EQ(arabic_unicode.size(), rsize)
    << "Arabic string codepoint size mismatch";
  
  rsize = utf::runes_size(chinese_bytes.begin(), chinese_bytes.end());
  EXPECT_EQ(chinese_unicode.size(), rsize)
    << "Chinese string codepoint size mismatch";
  
  rsize = utf::runes_size(invalid_bytes.begin(), invalid_bytes.end());
  EXPECT_EQ(0, rsize) << "Invalid string codepoint size mismatch";
}
  
TEST_F(utf_tests, TestCodepointSizeBuffer) {
  
  size_t rsize = utf::runes_size(russian_bytes.data());
  EXPECT_EQ(russian_unicode.size(), rsize)
    << "Russian string codepoint size mismatch";
  
  rsize = utf::runes_size(english_bytes.data());
  EXPECT_EQ(english_unicode.size(), rsize)
    << "English string codepoint size mismatch";
  
  rsize = utf::runes_size(arabic_bytes.data());
  EXPECT_EQ(arabic_unicode.size(), rsize)
    << "Arabic string codepoint size mismatch";
  
  rsize = utf::runes_size(chinese_bytes.data());
  EXPECT_EQ(chinese_unicode.size(), rsize)
    << "Chinese string codepoint size mismatch";
  
  rsize = utf::runes_size(invalid_bytes.data());
  EXPECT_EQ(error_unicode.size(), rsize)
    << "Invalid string codepoint size mismatch";
}
  
TEST_F(utf_tests, TestCodepointSizeSizedBuffer) {
  
  size_t rsize = utf::runes_size(russian_bytes.data(), russian_bytes.size());
  EXPECT_EQ(russian_unicode.size(), rsize)
    << "Russian string codepoint size mismatch";
  
  rsize = utf::runes_size(english_bytes.data(), english_bytes.size());
  EXPECT_EQ(english_unicode.size(), rsize)
    << "English string codepoint size mismatch";
  
  rsize = utf::runes_size(arabic_bytes.data(), arabic_bytes.size());
  EXPECT_EQ(arabic_unicode.size(), rsize)
    << "Arabic string codepoint size mismatch";
  
  rsize = utf::runes_size(chinese_bytes.data(), chinese_bytes.size());
  EXPECT_EQ(chinese_unicode.size(), rsize)
    << "Chinese string codepoint size mismatch";
  
  rsize = utf::runes_size(invalid_bytes.data(), invalid_bytes.size());
  EXPECT_EQ(error_unicode.size(), rsize)
    << "Invalid string codepoint size mismatch";
}
  
TEST_F(utf_tests, TestByteSize) {
  
  size_t size;
  size = utf::decoded_byte_size(english_unicode.begin(), english_unicode.end());
  EXPECT_EQ(english_bytes.size(), size) << "English byte size mismtach";
  
  size = utf::decoded_byte_size(russian_unicode.begin(), russian_unicode.end());
  EXPECT_EQ(russian_bytes.size(), size) << "Russian byte size mismatch";
  
  size = utf::decoded_byte_size(arabic_unicode.begin(), arabic_unicode.end());
  EXPECT_EQ(arabic_bytes.size(), size) << "Arabic byte size mismatch";
  
  size = utf::decoded_byte_size(error_unicode.begin(), error_unicode.end());
  EXPECT_EQ(error_byte_size, size)
    << "Invalid sequence byte size mismatch";
}
  
TEST_F(utf_tests, TestByteSizeBuffer) {
  
  size_t size;
  size = utf::decoded_byte_size(english_unicode.data(), english_unicode.size());
  EXPECT_EQ(english_bytes.size(), size) << "English byte size mismtach";
  
  size = utf::decoded_byte_size(russian_unicode.data(), russian_unicode.size());
  EXPECT_EQ(russian_bytes.size(), size) << "Russian byte size mismatch";
  
  size = utf::decoded_byte_size(arabic_unicode.data(), arabic_unicode.size());
  EXPECT_EQ(arabic_bytes.size(), size) << "Arabic byte size mismatch";
}
  
TEST_F(utf_tests, TestDecodeCodepoints) {
  utf::rune cp;
  int nbytes;
  std::string str;
  
  str = { 'a' };
  nbytes = utf::next(str.begin(), str.end(), cp);
  ASSERT_EQ(str.size(), nbytes);
  ASSERT_EQ(0x61, cp);
  
  str = { char(0xd0), char(0xb1) };
  nbytes = utf::next(str.begin(), str.end(), cp);
  ASSERT_EQ(str.size(), nbytes);
  ASSERT_EQ(0x431, cp);
  
  str = { char(0xd1), char(0x81) };
  nbytes = utf::next(str.begin(), str.end(), cp);
  ASSERT_EQ(str.size(), nbytes);
  ASSERT_EQ(0x441, cp);
  
  str = { char(0xd8), char(0xac) };
  nbytes = utf::next(str.begin(), str.end(), cp);
  ASSERT_EQ(str.size(), nbytes);
  ASSERT_EQ(0x62c, cp);
  
  str = { char(0xe6), char(0x9c), char(0x89) };
  nbytes = utf::next(str.begin(), str.end(), cp);
  ASSERT_EQ(str.size(), nbytes);
  ASSERT_EQ(0x6709, cp);
}
  
TEST_F(utf_tests, TestDecodeEnglish) {
  utf::ustring ustr;
  utf::decode(english_bytes.begin(), english_bytes.end(),
                     std::back_inserter(ustr));
  EXPECT_THAT(ustr, ContainerEq(english_unicode));
}
  
TEST_F(utf_tests, TestEncodeEnglish) {
  std::string dstr;
  utf::encode(english_unicode.begin(), english_unicode.end(),
                     std::back_inserter(dstr));
  EXPECT_THAT(dstr, ContainerEq(english_bytes));
}
  
TEST_F(utf_tests, TestDecodeRussian) {
  utf::ustring ustr;
  utf::decode(russian_bytes.begin(), russian_bytes.end(),
                     std::back_inserter(ustr));
  EXPECT_THAT(ustr, ContainerEq(russian_unicode));
}
  
TEST_F(utf_tests, TestEncodeRussian) {
  std::string dstr;
  utf::encode(russian_unicode.begin(), russian_unicode.end(),
                     std::back_inserter(dstr));
  EXPECT_THAT(dstr, ContainerEq(russian_bytes));
}
  
TEST_F(utf_tests, TestDecodeArabic) {
  utf::ustring ustr;
  utf::decode(arabic_bytes.begin(), arabic_bytes.end(),
                     std::back_inserter(ustr));
  EXPECT_THAT(ustr, ContainerEq(arabic_unicode));
}
  
TEST_F(utf_tests, TestEncodeArabic) {
  std::string dstr;
  utf::encode(arabic_unicode.begin(), arabic_unicode.end(),
                     std::back_inserter(dstr));
  EXPECT_THAT(dstr, ContainerEq(arabic_bytes));
}
  
TEST_F(utf_tests, TestDecodeChinese) {
  utf::ustring ustr;
  utf::decode(chinese_bytes.begin(), chinese_bytes.end(),
                     std::back_inserter(ustr));
  EXPECT_THAT(ustr, ContainerEq(chinese_unicode));
}
  
TEST_F(utf_tests, TestEncodeChinese) {
  std::string dstr;
  utf::encode(chinese_unicode.begin(), chinese_unicode.end(),
                     std::back_inserter(dstr));
  EXPECT_THAT(dstr, ContainerEq(chinese_bytes));
}
  
TEST_F(utf_tests, TestDecodeInvalid) {
  utf::ustring ustr;
  utf::decode(invalid_bytes.begin(), invalid_bytes.end(),
                     std::back_inserter(ustr));
  EXPECT_THAT(ustr, ContainerEq(error_unicode));
}
  
TEST_F(utf_tests, TestEncodeInvalid) {
  std::string dstr;
  utf::encode(invalid_bytes.begin(), invalid_bytes.end(),
                     std::back_inserter(dstr));
  EXPECT_THAT(dstr, ContainerEq(errorstring));
}
  
  
TEST_F(utf_tests, TestDecodeEnglishBuffer) {
  utf::rune buffer[1024];
  utf::decode(english_bytes.data(), buffer);
  EXPECT_THAT(utf::ustring(buffer, english_unicode.size()),
              ContainerEq(english_unicode));
}
  
TEST_F(utf_tests, TestEncodeEnglishBuffer) {
  char buffer[1024];
  utf::encode(english_unicode.data(), buffer);
  EXPECT_THAT(std::string(buffer, english_bytes.size()),
              ContainerEq(english_bytes));
}

TEST_F(utf_tests, TestDecodeRussianBuffer) {
  utf::rune buffer[1024];
  utf::decode(russian_bytes.data(), buffer);
  EXPECT_THAT(utf::ustring(buffer, russian_unicode.size()),
              ContainerEq(russian_unicode));
}
  
TEST_F(utf_tests, TestEncodeRussianBuffer) {
  char buffer[1024];
  utf::encode(russian_unicode.data(), buffer);
  EXPECT_THAT(std::string(buffer, russian_bytes.size()),
              ContainerEq(russian_bytes));
}
  
TEST_F(utf_tests, TestDecodeArabicBuffer) {
  utf::rune buffer[1024];
  utf::decode(arabic_bytes.data(), buffer);
  EXPECT_THAT(utf::ustring(buffer, arabic_unicode.size()),
              ContainerEq(arabic_unicode));
}
  
TEST_F(utf_tests, TestEncodeArabicBuffer) {
  char buffer[1024];
  utf::encode(arabic_unicode.data(), buffer);
  EXPECT_THAT(std::string(buffer, arabic_bytes.size()),
              ContainerEq(arabic_bytes));
}
  
TEST_F(utf_tests, TestDecodeChineseBuffer) {
  utf::rune buffer[1024];
  utf::decode(chinese_bytes.data(), buffer);
  EXPECT_THAT(utf::ustring(buffer, chinese_unicode.size()),
              ContainerEq(chinese_unicode));
}
  
TEST_F(utf_tests, TestEncodeChineseBuffer) {
  char buffer[1024];
  utf::encode(chinese_unicode.data(), buffer);
  EXPECT_THAT(std::string(buffer, chinese_bytes.size()),
              ContainerEq(chinese_bytes));
}
  
TEST_F(utf_tests, TestDecodeInvalidBuffer) {
  utf::rune buffer[1024];
  utf::decode(invalid_bytes.data(), buffer);
  EXPECT_THAT(utf::ustring(buffer, error_unicode.size()),
              ContainerEq(error_unicode));
}
  
TEST_F(utf_tests, TestEncodeInvalidBuffer) {
  char buffer[1024];
  utf::encode(invalid_bytes.data(), buffer);
  EXPECT_THAT(std::string(buffer, errorstring.size()),
              ContainerEq(errorstring));
}
  


TEST_F(utf_tests, TestDecodeEnglishSizedBuffer) {
  utf::rune buffer[1024];
  utf::decode(english_bytes.data(), english_bytes.size(), buffer);
  EXPECT_THAT(utf::ustring(buffer, english_unicode.size()),
              ContainerEq(english_unicode));
}
  
TEST_F(utf_tests, TestEncodeEnglishSizedBuffer) {
  char buffer[1024];
  utf::encode(english_unicode.data(), english_unicode.size(), buffer);
  EXPECT_THAT(std::string(buffer, english_bytes.size()),
              ContainerEq(english_bytes));
}

TEST_F(utf_tests, TestDecodeRussianSizedBuffer) {
  utf::rune buffer[1024];
  utf::decode(russian_bytes.data(), russian_bytes.size(), buffer);
  EXPECT_THAT(utf::ustring(buffer, russian_unicode.size()),
              ContainerEq(russian_unicode));
}
  
TEST_F(utf_tests, TestEncodeRussianSizedBuffer) {
  char buffer[1024];
  utf::encode(russian_unicode.data(), russian_unicode.size(), buffer);
  EXPECT_THAT(std::string(buffer, russian_bytes.size()),
              ContainerEq(russian_bytes));
}
  
TEST_F(utf_tests, TestDecodeArabicSizedBuffer) {
  utf::rune buffer[1024];
  utf::decode(arabic_bytes.data(), arabic_bytes.size(), buffer);
  EXPECT_THAT(utf::ustring(buffer, arabic_unicode.size()),
              ContainerEq(arabic_unicode));
}
  
TEST_F(utf_tests, TestEncodeArabicSizedBuffer) {
  char buffer[1024];
  utf::encode(arabic_unicode.data(), arabic_unicode.size(), buffer);
  EXPECT_THAT(std::string(buffer, arabic_bytes.size()),
              ContainerEq(arabic_bytes));
}
  
TEST_F(utf_tests, TestDecodeChineseSizedBuffer) {
  utf::rune buffer[1024];
  utf::decode(chinese_bytes.data(), chinese_bytes.size(), buffer);
  EXPECT_THAT(utf::ustring(buffer, chinese_unicode.size()),
              ContainerEq(chinese_unicode));
}
  
TEST_F(utf_tests, TestEncodeChineseSizedBuffer) {
  char buffer[1024];
  utf::encode(chinese_unicode.data(), chinese_unicode.size(), buffer);
  EXPECT_THAT(std::string(buffer, chinese_bytes.size()),
              ContainerEq(chinese_bytes));
}
  
TEST_F(utf_tests, TestDecodeInvalidSizedBuffer) {
  utf::rune buffer[1024];
  utf::decode(invalid_bytes.data(), invalid_bytes.size(), buffer);
  EXPECT_THAT(utf::ustring(buffer, error_unicode.size()),
              ContainerEq(error_unicode));
}
  
TEST_F(utf_tests, TestEncodeInvalidSizedBuffer) {
  char buffer[1024];
  utf::encode(invalid_bytes.data(), invalid_bytes.size(), buffer);
  EXPECT_THAT(std::string(buffer, errorstring.size()),
              ContainerEq(errorstring));
}
  
TEST_F(utf_tests, TestFindCodepoint) {
  
  using namespace utf;
  
  EXPECT_EQ(russian_bytes.begin(),
            find(russian_bytes.begin(), russian_bytes.end(), 'I'));
  
  EXPECT_EQ(russian_bytes.begin() + 4,
            find(russian_bytes.begin(), russian_bytes.end(), 'p'));
  {
    // б = 1073
    auto it = utf::find(russian_bytes.begin(), russian_bytes.end(), 1073);
    auto end = russian_bytes.end();
    EXPECT_TRUE(it != end);
    if (it != end) {
      int n;
      utf::rune r = utf::next_rune(it, end, n);
      EXPECT_EQ(1073, r);
    }
  }
  
  {
    std::string s{ u8"\xff\x4\x61" }; // a = 0x61
    auto it = utf::find(s.begin(), s.end(), 'a');
    auto end = s.end();
    EXPECT_TRUE(it != end);
    if (it != end) {
      int n;
      utf::rune r = utf::next_rune(it, end, n);
      EXPECT_EQ('a', r);
    }
  }
  
  {
    std::string s{ u8"\xff\x4\xd0\xb1" }; // б = 0xd0 0xb1 = 1073
    auto it = utf::find(s.begin(), s.end(), 1073);
    auto end = s.end();
    EXPECT_TRUE(it != end);
    if (it != end) {
      int n;
      utf::rune r = utf::next_rune(it, end, n);
      EXPECT_EQ(1073, r);
    }
  }
}
  
TEST_F(utf_tests, TestFindCodepointWithBuffers) {
  
  using namespace utf;
  
  EXPECT_EQ(russian_bytes.data(), find(russian_bytes.data(), 'I'));
  
  EXPECT_EQ(russian_bytes.data() + 4, find(russian_bytes.data(), 'p'));
  
  {
    // б = 1073
    auto * ptr = find(russian_bytes.data(), 1073);
    ASSERT_TRUE(ptr);
    utf::rune cp;
    utf::next(ptr, cp);
    EXPECT_EQ(1073, cp);
  }
  
  {
    std::string s{ u8"\xff\x4\x61" }; // a = 0x61
    auto * ptr = find(s.data(), 'a');
    ASSERT_TRUE(ptr);
    utf::rune cp;
    utf::next(ptr, cp);
    EXPECT_EQ('a', cp);
  }
  
  {
    std::string s{ u8"\xff\x4\xd0\xb1" }; // б = 0xd0 0xb1 = 1073
    auto * ptr = find(s.data(), 1073);
    ASSERT_TRUE(ptr);
    utf::rune cp;
    utf::next(ptr, cp);
    EXPECT_EQ(1073, cp);
  }
}
  
TEST_F(utf_tests, TestFindCodepointWithSizedBuffers) {
  
  using namespace utf;
  
  EXPECT_EQ(russian_bytes.data(),
            find(russian_bytes.data(), russian_bytes.size(), 'I'));
  
  EXPECT_EQ(russian_bytes.data() + 4,
            find(russian_bytes.data(), russian_bytes.size(), 'p'));
  
  {
    // б = 1073
    auto * ptr = find(russian_bytes.data(), russian_bytes.size(), 1073);
    EXPECT_TRUE(ptr);
    if (ptr) {
      utf::rune cp;
      utf::next(ptr, cp);
      EXPECT_EQ(1073, cp);
    }
  }
  
  {
    std::string s{ u8"\xff\x4\x61" }; // a = 0x61
    auto * ptr = find(s.data(), s.size(), 'a');
    EXPECT_TRUE(ptr);
    if (ptr) {
      utf::rune cp;
      utf::next(ptr, cp);
      EXPECT_EQ('a', cp);
    }
  }
  
  {
    std::string s{ u8"\xff\x4\xd0\xb1" }; // б = 0xd0 0xb1 = 1073
    auto * ptr = find(s.data(), s.size(), 1073);
    EXPECT_TRUE(ptr);
    if (ptr) {
      utf::rune cp;
      utf::next(ptr, cp);
      EXPECT_EQ(1073, cp);
    }
  }
}

TEST_F(utf_tests, TestReverseFindInBytes) {
  
  {
    std::string main{ u8"aabbccdd" };
    auto expected = main.begin() + 5;
    auto it = utf::rfind(main.begin(), main.end(), 'c');
    ASSERT_TRUE(it != main.end());
    EXPECT_EQ(expected, it);
  }
  
  {
    // каззла | з = 1079
    std::string main{ u8"\xd0\xba\xd0\xb0\xd0\xb7\xd0\xb7\xd0\xbb\xd0\xb0" };
    auto expected = main.begin() + 6;
    auto it = utf::rfind(main.begin(), main.end(), 1079);
    ASSERT_TRUE(it != main.end());
    EXPECT_EQ(expected, it);
  }
}
  
TEST_F(utf_tests, TestReverseFindInBytesWithBuffers) {
  
  {
    std::string main{ u8"aabbccdd" };
    auto * expected = main.data() + 5;
    auto * ptr = utf::rfind(main.data(), 'c');
    ASSERT_TRUE(ptr);
    EXPECT_EQ(expected, ptr);
  }
  
  {
    // каззла | з = 1079
    std::string main{ u8"\xd0\xba\xd0\xb0\xd0\xb7\xd0\xb7\xd0\xbb\xd0\xb0" };
    auto * expected = main.data() + 6;
    auto * ptr = utf::rfind(main.data(), 1079);
    ASSERT_TRUE(ptr);
    EXPECT_EQ(expected, ptr);
  }
}
  
TEST_F(utf_tests, TestReverseFindInBytesWithSizedBuffers) {
  
  {
    std::string main{ u8"aabbccdd" };
    auto * expected = main.data() + 5;
    auto * ptr = utf::rfind(main.data(), main.size(), 'c');
    ASSERT_TRUE(ptr);
    EXPECT_EQ(expected, ptr);
  }
  
  {
    // каззла | з = 1079
    std::string main{ u8"\xd0\xba\xd0\xb0\xd0\xb7\xd0\xb7\xd0\xbb\xd0\xb0" };
    auto * expected = main.data() + 6;
    auto * ptr = utf::rfind(main.data(), main.size(), 1079);
    ASSERT_TRUE(ptr);
    EXPECT_EQ(expected, ptr);
  }
}
  
TEST_F(utf_tests, TestPredicates) {
  
  using namespace utf;
  
  EXPECT_TRUE(1047 == upper(1079)); // з
  EXPECT_TRUE(1079 == lower(1047)); // з
  EXPECT_TRUE(1047 == title(1079)); // з
  
  EXPECT_TRUE( is_upper(1047)); // з
  EXPECT_FALSE(is_upper(1079)); // з
  
  EXPECT_FALSE(is_lower(1047)); // з
  EXPECT_TRUE( is_lower(1079)); // з
  
  EXPECT_TRUE( is_title(1047)); // з
  EXPECT_FALSE(is_title(1079)); // з
  
  EXPECT_TRUE( is_alpha(1047)); // з
  EXPECT_TRUE( is_alpha(1079)); // з
  
  EXPECT_FALSE(is_digit(1047)); // з
  EXPECT_FALSE(is_digit(1079)); // з
  
  EXPECT_FALSE(is_ideographic(1047)); // з
  EXPECT_FALSE(is_ideographic(1079)); // з
  
  EXPECT_FALSE(is_space(1047)); // з
  EXPECT_FALSE(is_space(1079)); // з
  
  
  EXPECT_TRUE('A' == upper('a'));
  EXPECT_TRUE('a' == lower('A'));
  EXPECT_TRUE('A' == title('a'));
  
  EXPECT_TRUE( is_upper('A'));
  EXPECT_FALSE(is_upper('a'));
  
  EXPECT_FALSE(is_lower('A'));
  EXPECT_TRUE( is_lower('a'));
  
  EXPECT_TRUE( is_title('A'));
  EXPECT_FALSE(is_title('a'));
  
  EXPECT_TRUE( is_alpha('A'));
  EXPECT_TRUE( is_alpha('a'));
  
  EXPECT_FALSE(is_digit('A'));
  EXPECT_FALSE(is_digit('a'));
  
  EXPECT_FALSE(is_ideographic('A'));
  EXPECT_FALSE(is_ideographic('a'));
  
  EXPECT_FALSE(is_space('A'));
  EXPECT_FALSE(is_space('a'));
  
  
  EXPECT_TRUE(1588 == upper(1588)); // ش
  EXPECT_TRUE(1588 == lower(1588)); // ش
  EXPECT_TRUE(1588 == title(1588)); // ش
  EXPECT_FALSE(is_upper(1588)); // ش
  EXPECT_FALSE(is_lower(1588)); // ش
  EXPECT_FALSE(is_title(1588)); // ش
  EXPECT_TRUE(is_alpha(1588)); // ش
  EXPECT_FALSE(is_digit(1588)); // ش
  EXPECT_FALSE(is_ideographic(1588)); // ش
  EXPECT_FALSE(is_space(1588)); // ش
  
  EXPECT_FALSE(is_alpha('2'));
  EXPECT_TRUE(is_digit('2'));
  EXPECT_FALSE(is_upper('2'));
  EXPECT_FALSE(is_lower('2'));
  EXPECT_FALSE(is_title('2'));
  EXPECT_FALSE(is_ideographic('2'));
  EXPECT_FALSE(is_ideographic('2'));
  EXPECT_TRUE('2' == upper('2'));
  EXPECT_TRUE('2' == lower('2'));
  EXPECT_TRUE('2' == title('2'));
  
  
  EXPECT_FALSE(is_alpha(1634)); // ٢
  EXPECT_TRUE(is_digit(1634)); // ٢
  EXPECT_FALSE(is_upper(1634)); // ٢
  EXPECT_FALSE(is_lower(1634)); // ٢
  EXPECT_FALSE(is_title(1634)); // ٢
  EXPECT_FALSE(is_ideographic(1634)); // ٢
  EXPECT_TRUE(1634 == upper(1634)); // ٢
  EXPECT_TRUE(1634 == lower(1634)); // ٢
  EXPECT_TRUE(1634 == title(1634)); // ٢
  
  EXPECT_TRUE(is_space(' '));
}
  
TEST_F(utf_tests, TestErrorRuneValid) {
  EXPECT_TRUE(utf::is_valid_rune(
    std::begin(utf::error_bytes), std::end(utf::error_bytes)));
}
  
TEST_F(utf_tests, TestRuneIsValid) {
  
  EXPECT_TRUE(is_valid('A'));
  EXPECT_TRUE(is_valid('z'));
  EXPECT_TRUE(is_valid('0'));
  EXPECT_TRUE(is_valid('9'));
  EXPECT_TRUE(is_valid('\0'));
  EXPECT_TRUE(russian_unicode[0]);
  EXPECT_TRUE(*(russian_unicode.end() - 1));
  EXPECT_TRUE(arabic_unicode[0]);
  EXPECT_TRUE(*(arabic_unicode.end() - 1));
  EXPECT_TRUE(chinese_unicode[0]);
  EXPECT_TRUE(*(chinese_unicode.end() - 1));
  
  // First valid sequences in their ranges
  EXPECT_TRUE(is_valid(0x00000000));
  EXPECT_TRUE(is_valid(0x00000080));
  EXPECT_TRUE(is_valid(0x00000800));
  EXPECT_TRUE(is_valid(0x00010000));
  EXPECT_FALSE(is_valid(0x00200000));
  EXPECT_FALSE(is_valid(0x04000000));
  
  // Last valid sequences in their ranges
  EXPECT_TRUE(is_valid(0x0000007F));
  EXPECT_TRUE(is_valid(0x000007FF));
  EXPECT_TRUE(is_valid(0x0000FFFF));
  EXPECT_FALSE(is_valid(0x001FFFFF));
  EXPECT_FALSE(is_valid(0x03FFFFFF));
  EXPECT_FALSE(is_valid(0x7FFFFFFF));
  
  // Other boundary conditions
  EXPECT_TRUE(is_valid(0x0000F7FF));
  EXPECT_TRUE(is_valid(0x0000E000));
  EXPECT_TRUE(is_valid(0x0000FFFD));
  EXPECT_TRUE(is_valid(0x0010FFFF));
  EXPECT_FALSE(is_valid(0x00110000));
  
  EXPECT_TRUE(is_valid(0x80));
}
  
TEST_F(utf_tests, TestIsTrailingBytes) {
  
  auto check = [](auto x) -> bool {
    return (x & 0xC0) == 0x80; // always in [0x80, 0xBF]
  };
  
  for (auto x : english_bytes)
    EXPECT_EQ(check(x), is_trail(x)) << std::bitset<8>(x);
  
  for (auto x : russian_bytes)
    EXPECT_EQ(check(x), is_trail(x)) << std::bitset<8>(x);
  
  for (auto x : arabic_bytes)
    EXPECT_EQ(check(x), is_trail(x)) << std::bitset<8>(x);
  
  for (auto x : chinese_bytes)
    EXPECT_EQ(check(x), is_trail(x)) << std::bitset<8>(x);
  
  EXPECT_TRUE(is_trail(0x80));  // 10 00 00 00 trailing
  EXPECT_FALSE(is_trail(0xC0)); // 11 00 00 00 multi-byte start
  EXPECT_FALSE(is_trail(0x60)); // 01 10 00 00 one-byte start
  EXPECT_FALSE(is_trail(0xFF)); // 11 11 11 11 one-byte start
}
  
TEST_F(utf_tests, TestIsSafe) {
  
  // [0x0000, 0x001F] and 0x007F C0 control characters except HT, LF, CR
  // 0x09, 0x0A, 0x0D
  for (unsigned int x = 0x0; x <= 0x001F; ++x) {
    if (x == 0x09 || x == 0x0A || x == 0x0D)
      EXPECT_TRUE(is_safe(x)) << "should be safe: " << x;
    else
      EXPECT_FALSE(is_safe(x)) << "should be unsafe: " << x;
  }
  
  EXPECT_FALSE(is_safe(0x007F)) << "should be unsafe: 0x007F";
  
  // C1 set
  for (unsigned int x = 0x0080; x <= 0x009F; ++x) {
    EXPECT_FALSE(is_safe(x)) << "should be unsafe: " << x;
  }
  
  for (auto x : english_bytes)
    EXPECT_TRUE(is_safe(x)) << "should be safe: " << x;
  
  for (auto x : russian_bytes)
    EXPECT_TRUE(is_safe(x))  << "should be safe: " << x;
  
  for (auto x : arabic_bytes)
    EXPECT_TRUE(is_safe(x))  << "should be safe: " << x;
  
  for (auto x : chinese_bytes)
    EXPECT_TRUE(is_safe(x))  << "should be safe: " << x;
  
  EXPECT_TRUE(is_safe(error_rune));
}
  
TEST_F(utf_tests, ByteSizeFromLead) {
  
  EXPECT_EQ(1, byte_size_from_lead('a'));
  EXPECT_EQ(1, byte_size_from_lead('Z'));
  EXPECT_EQ(1, byte_size_from_lead('0'));
  EXPECT_EQ(1, byte_size_from_lead('9'));
  
  EXPECT_EQ(2, byte_size_from_lead(0xD0));     // л  0xD0 0xBB
  EXPECT_EQ(2, byte_size_from_lead(0xD8));     // sh 0xD8 0xB4 (arabic)
  EXPECT_EQ(3, byte_size_from_lead(0xE9));     // 阴 0xE9 0x98 0xB4
  EXPECT_EQ(3, byte_size_from_lead(0xEF));     //    0xEF 0xBF 0xBD
  EXPECT_EQ(4, byte_size_from_lead(0xF0));     //    0xF0 0x98 0xA0 0x81
  EXPECT_EQ(4, byte_size_from_lead(0xF4));     // Mx 0xF4 0x8F 0xBF 0xBF
}
  
TEST_F(utf_tests, TestToBytes) {
  
  using namespace ::testing;
  
  u8 buf[4];
  int size;
  
  auto to_vector = [&buf](int size) {
    std::vector<u8> v;
    for (int i = 0; i < size; ++i) v.push_back(buf[i]);
    return v;
  };
  
  auto ref = [](std::initializer_list<u8> ilist) {
    return std::vector<u8> { ilist };
  };
  
  size = to_bytes('a', (u8*) buf);
  EXPECT_EQ(1, size);
  if (size == 1)
    EXPECT_THAT(to_vector(1), ContainerEq(ref({ 0x61 })));
  
  size = to_bytes(0x43b, (u8*) buf); // л
  EXPECT_EQ(2, size);
  if (size == 2)
    EXPECT_THAT(to_vector(2), ContainerEq(ref({ 0xD0, 0xBB })));
  
  size = to_bytes(0x634, (u8*) buf); // ش
  EXPECT_EQ(2, size);
  if (size == 2)
    EXPECT_THAT(to_vector(2), ContainerEq(ref({ 0xD8, 0xB4 })));
  
  size = to_bytes(0x9634, (u8*) buf); // 阴
  EXPECT_EQ(3, size);
  if (size == 3)
    EXPECT_THAT(to_vector(3), ContainerEq(ref({ 0xE9, 0x98, 0xB4 })));
  
  size = to_bytes(error_rune, (u8*) buf);
  EXPECT_EQ(3, size);
  if (size == 3)
    EXPECT_THAT(to_vector(3), ContainerEq(ref({ 0xef, 0xbf, 0xbd })));
  
  size = to_bytes(0x18801, (u8*) buf); // Tangut Component
  EXPECT_EQ(4, size);
  if (size == 4)
    EXPECT_THAT(to_vector(4), ContainerEq(ref({ 0xF0, 0x98, 0xA0, 0x81 })));
  
  size = to_bytes(0x10FFFF, (u8*) buf); // Max rune
  EXPECT_EQ(4, size);
  if (size == 4)
    EXPECT_THAT(to_vector(4), ContainerEq(ref({ 0xF4, 0x8F, 0xBF, 0xBF })));
  
  size = to_bytes(0x10FFFF + 1, (u8*) buf); // Invalid codepoint => error_rune
  EXPECT_EQ(3, size);
  if (size == 3)
    EXPECT_THAT(to_vector(3), ContainerEq(ref({  0xef, 0xbf, 0xbd })));
}
  
TEST_F(utf_tests, ByteSizeFromCodepoint) {
  
  EXPECT_EQ(1, byte_size('a'));
  EXPECT_EQ(1, byte_size('Z'));
  EXPECT_EQ(1, byte_size('0'));
  EXPECT_EQ(1, byte_size('9'));
  
  EXPECT_EQ(2, byte_size(0x43b));      // л
  EXPECT_EQ(2, byte_size(0x634));      // ش
  EXPECT_EQ(3, byte_size(0x9634));     // 阴
  EXPECT_EQ(3, byte_size(error_rune));
  EXPECT_EQ(4, byte_size(0x18801));    // Tangut Component
  EXPECT_EQ(4, byte_size(0x10FFFF));   // Mx 0xF4 0x8F 0xBF 0xBF
  EXPECT_EQ(0, byte_size(0x10FFFF + 1));
}
  
} /* namespace dasky */
