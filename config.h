//
//  config.h
//  dasky
//
//  Created by Kareem Alkaseer on 08/12/2017.
//
//

#ifndef dasky_utf_config_h
#define dasky_utf_config_h


# if defined(DASKY_OUT_OF_TREE_BUILD)

# if defined(__clang__)
#  define DASKY_CLANG
#  if !__has_feature(cxx_thread_local)
#    define DASKY_NO_THREAD_LOCAL
#  endif
#
# elif defined(__INTEL_COMPILER) || defined(__ICC) || defined(__ICL)
#  define DASKY_ICC
#
# elif defined(__GNUC__) || defined(__GNUG__)
#  DASKY_GCC_DEFINES
#
# ifdef __APPLE__
#   define DASKY_NO_THREAD_LOCAL
# endif
#
# elif defined(_MSC_VER)
#  define DASKY_MSVC
#
# elif defined(__xlc__)  || defiend (__xlC__)  || \
       defined(__IBMC__) || defined(__IBMCPP__)
#  define DASKY_XLC
# 
# elif defined(__SUNPRO_C) || defined (__SUNPRO_CC)
#  define DASKY_SUNSTUDEO
# endif
# if defined(DASKY_GCC)
#   if defined(__cplusplus) && (DASKY_CPP_VERSION > 3)
#     define opt_always_inline [[ gnu::always_inline ]]
#   else
#     define opt_always_inline __attribute__(always_inline)
#   endif
#
# elif __has_attribute(always_inline)
#   if defined(__cplusplus) && (DASKY_CPP_VERSION > 3)
#     define opt_always_inline [[ gnu::always_inline ]]
#   else
#     define opt_always_inline __attribute__(always_inline)
#   endif
# elif defined(DASKY_MSVC)
#   define opt_always_inline __forceinline
# else
#   define opt_always_inline
# endif
#
# if defined(DASKY_CLANG)
#   if defined(__cplusplus)
#     if (DASKY_CPP_VERSION >= 17) && __has_cpp_attribute(noreturn)
#       define opt_noreturn [[noreturn]]
#     elif __has_cpp_attribute(gnu::noreturn)
#       define opt_noreturn [[gnu::noreturn]]
#     else
#       define opt_noreturn __attribute__((noreurn))
#     endif
#   endif
#
# elif defined(DASKY_GCC) || defined (DASKY_ICC)
#   if defined(__cplusplus) && (DASKY_CPP_VERSION >= 17)
#     define opt_noreturn [[noreturn]]
#   elif defined(__cplusplus) && (DASKY_CPP_VERSION > 3) && \
         ((__GNUC__ >= 4) || ((__GNU__ == 4) && (__GNUC__MINOR >= 8)))
#     define opt_noreturn [[gnu::noreturn]]
#   else
#     define opt_noreturn __attribute__((noreturn))
#   endif
#
# elif defined(DASKY_MSVC)
#   if defined(__cplusplus) && (_MSC_VER >= 1700)
#     define opt_noreturn [[noreturn]]
#   else
#     define opt_noreturn 
#   endif
#
# else
#   define opt_noreturn
# endif
#
# if defined(DASKY_GCC)
#   define likely(x)   (__builtin_expect(!!(x), 1))
#   define unlikely(x) (__builtin_expect(!!(x), 0))
# elif __has_builtin(__builtin_expect)
#   define likely(x)   (__builtin_expect(!!(x), 1))
#   define unlikely(x) (__builtin_expect(!!(x), 0))
# else
#   define likely(x)    (x)
#   define unlikely(x)  (x)
# endif


#include <stddef.h>  /* for NULL, size_t */

#if !(defined(_MSC_VER) && (_MSC_VER < 1600))
#include <stdint.h>  /* for uintptr_t */
#endif

#define HAS_INT64  /* For now it is always defined */

#if !defined(_MSC_VER)

#include <stdint.h>

typedef int8_t              i8;
typedef int16_t             i16;
typedef int32_t             i32;
typedef uint8_t             u8;
typedef uint16_t            u16;
typedef uint32_t            u32;

# if defined(OSX) || defined(__APPLE__)
   typedef uint64_t         u64;
   typedef int64_t          i64;
#  define HAS_INT64
#  ifndef INT64_C
#   define INT64_C(x) x ## LL
#  endif
#  ifndef UINT64_C
#   define UINT64_C(x) x ## ULL
#  endif
#  define INT64_F "l"

# elif defined(__LP64__)
   typedef unsigned long    u64;  // NOLINT
   typedef long             i64;  // NOLINT
#  define HAS_INT64
#  ifndef INT64_C
#   define INT64_C(x) x ## L
#  endif
#  ifndef UINT64_C
#   define UINT64_C(x) x ## UL
#  endif
#  define INT64_F "l"

# else
   typedef uint64_t         u64;
   typedef int64_t          i64;
#  define HAS_INT64
#  ifndef INT64_C
#   define INT64_C(x) x ## LL
#  endif
#  ifndef UINT64_C
#   define UINT64_C(x) x ## ULL
#  endif
#  define INT64_F "ll"

# endif


#else /* _MSC_VER */

/* Define C99 equivalent types, since MSVC doesn't provide stdint.h. */
typedef signed char         i8;
typedef signed short        i16;
typedef signed int          i32;
typedef __int64             i64;
typedef unsigned char       u8;
typedef unsigned short      u16;
typedef unsigned int        u32;
typedef unsigned __int64    u64;

#define HAS_INT64

#ifndef INT64_C
# define INT64_C(x) x ## I64
#endif
#ifndef UINT64_C
# define UINT64_C(x) x ## UI64
#endif
#define INT64_F "I64"

#endif /* !_MSC_VER */

#if defined(__GNUC__) || defined(__clang__) || defined(__llvm__) || \
    defined(__INTEL_COMPILER) || defined(__ICC) || defined(__ICL)
# if defined(DASKY_HAS_INT128) && DASKY_HAS_INT128 != 1
#  undef DASKY_HAS_INT128
#  define DASKY_HAS_INT128 1
# endif
  typedef __int128 i128;
  typedef unsigned __int128 u128;
#else
# undef DASKY_HAS_INT128
#endif


# if defined (__cplusplus)
# include <iterator>
namespace utf {
namespace details {
  
  /// Advance a random access iterator in constant complexity.
  template< class Iter >
  inline void advance_iterator(
    Iter & it, std::size_t n, std::random_access_iterator_tag
  ) {
    it += n;
  }
  
  /// Advance an iterator using `std::advance`, which may be done
  /// in different complexities depending on the type of the
  /// type of the iterator.
  template< class Iter, class Tag >
  inline void advance_iterator(Iter & it, std::size_t n, Tag tag) {
    std::advance(it, n);
  }
} /* namespace details */

/// Advance an iterator based on its category for best efficiency.
/// Advancing a random access iterator is an O(1) operation.
template< class Iter >
inline void advance_iterator(Iter & it, std::size_t n) {
  details::advance_iterator(
    it, n, typename std::iterator_traits<Iter>::iterator_category());
}

/// Masks a single byte.
template< class T, class R = u8 >
opt_always_inline
constexpr R mask8(T x) noexcept {
  return static_cast<R>(0xFF & x);
}

/// Specialisation of mask8<u8, u8>.
template<> opt_always_inline
constexpr u8 mask8<u8, u8>(u8 x) noexcept { return x; }

/// Masks two bytes.
template< class T, class R = u16 >
opt_always_inline
constexpr T mask16(T x) noexcept {
  return static_cast<R>(0xFFFF & x);
}

/// Specialisation of mask16<u16, u16>.
template<> opt_always_inline
constexpr u16 mask16<u16, u16>(u16 x) noexcept { return x; }

} /* namespace utf */
#endif // defined (__cplusplus)

#undef BEGIN_DASKY_NAMESPACE
#undef END_DASKY_NAMESPACE

#define BEGIN_DASKY_NAMESPACE 
#define END_DASKY_NAMESPACE 

# else // DASKY BUILD
#  include "dasky/config.h"
#  include "dasky/basic_types.h"
#  include "dasky/iterator.h"
# endif


#endif /* dasky_utf_config_h */
