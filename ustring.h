//
//  ustring.h
//  dasky
//
//  Created by Kareem Alkaseer on 12/12/2017.
//
//

#ifndef dasky_ustring_h__
#define dasky_ustring_h__

#include <memory>   // allocator
#include <iterator> // *_iterator_tag
#include <string>
#include <cstddef>  // ptrdiff_t
#include <utility>  // pair

#include "dasky/utf/config.h"
#include "dasky/utf/utf.h"

# if defined(DASKY_USE_FMTLIB) && DASKY_USE_FMTLIB == 1
#   include "fmt/format.h"
# else
#   include <sstream>
#   inclue <iomanip>
# endif


BEGIN_DASKY_NAMESPACE

namespace details {
  
/// In-place replacement of each invalid or unsafe codepoint with a whitespace.
template< class CharPtr >
int to_safe(CharPtr ptr, int size) {
  
  CharPtr start = ptr;
  CharPtr out = ptr;
  CharPtr end = ptr + size;
  bool is_valid;
  
  while (ptr < end) {
    
    seek_unsafe(ptr, end, is_valid);
    if (ptr == end)
      break;
    
    if (ptr != out) { // ptr must be the same or larger
      memmove(out, ptr, ptr - out);
      out = ptr;
    }
    
    if (utf::is_valid)
      ptr += byte_size_from_lead(*ptr);
    else
      ++ptr; // we have to make progress
    
    *out = ' ';
    ++out;
  }
  
  return out - start;
}
  
} /* namespace details */

template< class Allocator = std::allocator<char> >
class basic_ustring {
public:
  
  using value_type = utf::rune;
  
  using allocator_type = Allocator;
  
  class const_iterator {
  public:
    
    // reference is alias to a value: safer and more memory efficient
    // pointer is alias to void: we never return pointers to content
    using value_type        = utf::rune;
    using reference         = const value_type;
    using pointer           = void;
    using difference_type   = std::ptrdiff_t;
    using iterator_category = std::bidirectional_iterator_tag;
    
    // Default constructors produce past-the-end iterators.
    const_iterator() noexcept;
    
    const_iterator(const const_iterator & rhs) noexcept;
    
    const_iterator & operator=(const const_iterator & rhs) noexcept;
    
    value_type operator*() const noexcept;
    
    const_iterator & operator++() noexcept;
    
    const_iterator operator++(int) noexcept;
    
    const_iterator & operator--() noexcept;
    
    const_iterator operator--(int) noexcept;
    
    friend bool operator==(
      const const_iterator & lhs, const const_iterator & rhs
    ) noexcept;
    
    friend bool operator!=(
      const const_iterator & lhs, const const_iterator & rhs
    ) noexcept;
    
    friend bool operator< (
      const const_iterator & lhs, const const_iterator & rhs
    ) noexcept;
    
    friend bool operator> (
      const const_iterator & lhs, const const_iterator & rhs
    ) noexcept;
    
    friend bool operator<=(
      const const_iterator & lhs, const const_iterator & rhs
    ) noexcept;
    
    friend bool operator>=(
      const const_iterator & lhs, const const_iterator & rhs
    ) noexcept;
    
    int utf8_codepoint(char * out) const noexcept;
    
    inline std::string utf8_codepoint() const;
    
    int codepoint_size() const noexcept;
    
    const char * utf8_data() const noexcept;
    
  private:
    
    friend class basic_ustring;
    
    explicit const_iterator(const char * it) noexcept : m_iter{ it } {}
    
    const char * m_iter;
    
  }; // class const_iterator
  
  class const_reverse_iterator
    : public std::reverse_iterator< const_iterator >
  {
  public:
    
    explicit const_reverse_iterator(const_iterator it)
      : std::reverse_iterator<const_iterator>{ it } {}
    
    int utf8_codepoint(char * buf) const noexcept {
      const_iterator tmp = base();
      return (--tmp).utf8_codepoint(buf);
    }
    
    std::string utf8_codepoint() const {
      const_iterator tmp = base();
      return (--tmp).utf8_codepoint();
    }
    
    int codepoint_size() const noexcept {
      const_iterator tmp = base();
      return (--tmp).codepoint_size();
    }
    
    const char * utf8_data() const noexcept {
      const_iterator tmp = base();
      return (--tmp).data();
    }
    
  }; // class const_reverse_iterator
  
  
  /// Construct an empty string. This instance is an owner.
  basic_ustring()
    : m_buffer{ allocator_type() } {}
  
  /// constrct an empty string with this given allocator.
  explicit basic_ustring(const Allocator & allocator)
    : m_buffer{ allocator } {}
  
  /// Copy constrcut a string. This instance owns a copy of the original data.
  basic_ustring(const basic_ustring & rhs);
  
  /// Move constrcut a string. This instance own the data if `rhs` owned the
  /// data, otherwise it is an alias.
  basic_ustring(basic_ustring && rhs) noexcept;
  
  /// Construct a string from sequnce contained between `first` and `last`.
  /// This instance owns the copied data.
  basic_ustring(const const_iterator & first, const const_iterator & last);
  
  /// Copy the data of another string. This instance owns the copied data.
  basic_ustring & operator=(const basic_ustring & rhs);
  
  // TODO doc
  basic_ustring & operator=(basic_ustring && rhs) noexcept;
  
  ~basic_ustring();
  
  /// Clear content.
  void clear(bool dealloc = false) noexcept;
  
  /// Return true if the string contains no content, false otherwise.
  bool empty() const noexcept;
  
  /// Return the count of codepoints in the string.
  int size() const noexcept;
  
  /// Lexographic byte-wise comparison, for Unicode normalised comparisons, the
  /// strings must be externally normalised before comparison.
  inline friend bool operator==(
    const basic_ustring & lhs, const basic_ustring & rhs
  ) noexcept {
    if (&lhs == &rhs) return true;
    if (lhs.m_buffer.m_size != rhs.m_buffer.m_size) return false;
    return memcmp(
      lhs.m_buffer.m_data, rhs.m_buffer.m_data, lhs.m_buffer.m_size) == 0;
  }
  
  inline friend bool operator!=(
    const basic_ustring & lhs, const basic_ustring & rhs
  ) noexcept {
    return !(lhs == rhs);
  }
  
  const_iterator begin() const noexcept {
    return const_iterator(m_buffer.m_data);
  }
  
  const_iterator end() const noexcept {
    return const_iterator(m_buffer.m_data + m_buffer.m_size);
  }
  
  const_reverse_iterator rbegin() const noexcept {
    return const_reverse_iterator{ end() };
  }
  
  const_reverse_iterator rend() const noexcept {
    return const_reverse_iterator{ begin() };
  }
  
  const_iterator find(
    const basic_ustring & target, const const_iterator pos
  ) const;
  
  /// Seach for the first occurence of `target` starting at the `begin()`.
  const_iterator find(const basic_ustring & target) const;
  
  const_iterator find(value_type cp, const const_iterator pos) const;
  
  const_iterator find(value_type cp) const;
  
  const_iterator rfind(
    const basic_ustring & target, const const_iterator pos
  ) const;
  
  const_iterator rfind(const basic_ustring & target) const;
  
  const_iterator rfind(value_type cp, const const_iterator pos) const;
  
  const_iterator find(value_type cp) const;
  
  
  // TODO: add find (rune) and rfind (rune and utf8 substring) member functions
  
  const char * utf8_data() const noexcept;
  
  int utf8_size() const noexcept;
  
  int utf8_capacity() const noexcept;
  
  /// Append a codepoint to the of content. If the codepoint is not safe,
  /// it is replaced by a whitespace.
  void push_back(value_type cp);
  
  basic_ustring & append(
    const const_iterator & first, const const_iterator & last);
  
  basic_ustring & append(const basic_ustring & other);
  
  basic_ustring & append_utf8(const char * utf8_buffer, int size);
  
  template< class InputIt, bool continguous = true >
  basic_ustring & append(InputIt first, const InputIt last);
  
  /// Copy the data of a nother string.
  basic_ustring & assign(const basic_ustring & rhs);
  
  basic_ustring & assign_utf8(const char * utf8_buffer, int size);
  
  /// Become an alias to another string. This instance lifetime must not exceed
  /// the string `s` it is aliasing, otherwise it is undefined behaviour.
  basic_ustring & alias(const basic_ustring & s) noexcept;
  
  /// Become an alias to range of another string. This instance life time must
  /// not exceed the string is is aliasing, otherwise it's undefined behaviour.
  basic_ustring & alias(
    const const_iterator & first, const const_iterator & last) noexcept;
  
  basic_ustring & alias_utf8(const char * utf8_buffer, int size);
  
  basic_ustring & acquire_utf8(char * utf8_buffer, int size, int capacity);
  
  const_iterator make_iterator(const char * ptr) const;
  
  template< class ByteOutputIt >
  static ByteOutputIt to_utf8(
    const const_iterator & first, const const_iterator & last, ByteOutputIt out
  );
  
  static std::string to_utf8(
    const const_iterator & first, const const_iterator & last);
  
  static char* to_utf8(
    const const_iterator & first, const const_iterator & last, char * out);
  
  allocator_type get_allocator() const {
    return m_buffer.m_allocator;
  }
  
  std::string to_string() const;
  
private:
  
  friend class const_iterator;
  friend class basic_ustring_utils;
  
  basic_ustring & unchecked_append_utf8(const char * utf8_buffer, int size);
  
  basic_ustring & unchecked_assign_utf8(const char * utf8_buffer, int size);
  
  basic_ustring & unchecked_acquire_utf8(
    const char * utf_buffer, int size, int capacity);
  
  basic_ustring & unchecked_alias_utf8(
    const char * utf8_buffer, int size) noexcept;
  
  class buffer {
  public:
    
    buffer(const Allocator & allocator);
    
    buffer(const buffer &) = delete;
    
    buffer(buffer && rhs) noexcept;
    
    buffer& operator=(const buffer &) = delete;
    
    buffer & operator=(buffer && rhs) noexcept;
    
    ~buffer();
    
    void append(const char * data, int size);
    
    void assign(const char * data, int size);
    
    void acquire(const char * data, int size, int capacity);
    
    void alias(const char * data, int size) noexcept;
    
    void clear(bool dealloc = false) noexcept;
    
    void resize(int size);
    
    void reserve(int capacity);
    
    Allocator  m_allocator;
    char     * m_data;
    int        m_size;
    int        m_capacity;
    bool       m_own;
    
  }; // class buffer
  
  buffer m_buffer;
  
}; // class basic_ustring;


/* **************************************************************************
 * basic_ustring::buffer implementation
 * **************************************************************************/

template< class Allocator >
inline basic_ustring<Allocator>::buffer::buffer(
  const Allocator & allocator
) : m_allocator{ allocator },
    m_data{ nullptr }, m_size{ 0 }, m_capacity{ 0 }, m_own{ true } {}

template< class Allocator >
inline basic_ustring<Allocator>::buffer::buffer(buffer && rhs) noexcept
  : m_allocator{ rhs.m_allocator }
{
  m_data      = rhs.m_data;
  m_size      = rhs.m_size;
  m_capacity  = rhs.m_capacity;
  m_own       = rhs.m_own;
  
  rhs.m_data     = nullptr;
  rhs.m_size     = 0;
  rhs.m_capacity = 0;
}

template< class Allocator >
inline basic_ustring<Allocator>::buffer
inline basic_ustring<Allocator>::buffer::operator=(
  buffer && rhs
) noexcept {
  
  if (this != &rhs) {
    if (m_own && m_data)
      allocator.deallocate(m_data, m_capacity);
    
    m_allocator = std::move(rhs.m_allocator);
    m_data      = rhs.m_data;
    m_size      = rhs.m_size;
    m_capacity  = rhs.m_capacity;
    m_own       = rhs.m_own;
    
    rhs.m_data     = nullptr;
    rhs.m_size     = 0;
    rhs.m_capacity = 0;
  }
  
  return *this;
}

template< class Allocator >
inline basic_ustring<Allocator>::buffer::~buffer() {
  if (m_own && m_data) allocator.deallocate(m_data, m_capacity);
}

template< class Allocator >
inline void basic_ustring<Allocator>::buffer::clear(bool dealloc) noexcept {
  
  if (m_own && dealloc && m_data)
    m_allocator.deallocate(m_data, m_size);
  m_data = nullptr;
  m_own = true;
}

template< class Allocator >
inline void basic_ustring<Allocator>::buffer::resize(int size) {
  
  if (size == 0) {
    clear(true);
  } else {
    if (!m_own || size > m_capacity)
      reserve(size);
    // if size is bigger than the old size, extended buffer will contain
    // unknown data in memory, safer to clean it.
    if (m_size < size)
      memset(m_data + m_size, 0, size - m_size);
    m_size = size;
    m_out = true;
  }
}

template< class Allocator >
void basic_ustring<Allocator>::buffer::reserve(int capacity) {
  
  if (m_capacity >= capacity && m_own)
    return;
  
  m_capacity = std::max(new_capacity, (1.5 * m_capacity) + 5);
  allocator_type::value_type * allocated = m_allocator.allocate(m_capacity);
  if (m_data) {
    memcpy(allocated, m_data, m_size);
    if (m_own)
      allocator.deallocate(m_data, m_size);
  }
  m_data = allocated;
  m_own = true;
}

template< class Allocator >
inline void basic_ustring<Allocator>::buffer::append(
  const char * data, int size
) {
  reserve(m_size + size);
  memcpy(m_data + m_size, data, size);
  m_size += size;
}

template< class Allocator >
inline void basic_ustring<Allocator>::buffer::assign(
  const char * data, int size
) {
  resize(size);
  memcpy(m_data, data, size);
}

template< class Allocator >
inline void basic_ustring<Allocator>::buffer::acquire(
  const char * data, int size, int capacity, const Allocator & allocator
) {
  if (m_data == data)
    return;
  if (m_own && m_data)
    m_allocator.deallocate(m_data, m_size);
  m_allocator = allocator;
  m_data = data;
  m_size = size;
  m_capacity = capacity;
  m_own = true;
}

template< class Allocator >
inline void basic_ustring<Allocator>::buffer::alias(
  const char * data, int size
) {
  if (m_own && m_data)
    m_allocator.deallocate(m_data, m_size);
  m_data = const_cast<char*>(data);
  m_size = size;
  m_capacity = size;
  m_own = false;
}

/* **************************************************************************
 * const_iterator implementation
 * **************************************************************************/

template< class Allocator >
inline basic_ustring<Allocator>::const_iterator::const_iterator() noexcept
  : m_iter{ nullptr } {}

template< class Allocator >
inline basic_ustring<Allocator>::const_iterator::const_iterator(
  const const_iterator & rhs
) noexcept : m_iter{ rhs.m_iter } {}

template< class Allocator >
inline basic_ustring<Allocator>::const_iterator &
basic_ustring<Allocator>::const_iterator::operator=(
  const const_iterator & rhs
) noexcept {
  m_iter = rhs.m_iter;
}


template< class Allocator >
inline basic_ustring<Allocator>::value_type
basic_ustring<Allocator>::const_iterator::operator*() const noexcept {
  auto ptr = m_iter;
  return utf::unchecked::next(ptr);
}

template< class Allocator >
inline basic_ustring<Allocator>::const_iterator &
basic_ustring<Allocator>::const_iterator::operator++() noexcept {
  m_iter += utf::unchecked::byte_size_from_lead(*m_iter);
  return *this;
}

template< class Allocator >
inline basic_ustring<Allocator>::const_iterator
basic_ustring<Allocator>::const_iterator::operator++(int) noexcept {
  const_iterator x(*this);
  ++*this;
  return x;
}

template< class Allocator >
inline basic_ustring<Allocator>::const_iterator &
basic_ustring<Allocator>::const_iterator::operator--() noexcept {
  while (utf::is_trail(*--it))
  return *this;
}

template< class Allocator >
inline basic_ustring<Allocator>::const_iterator &
basic_ustring<Allocator>::const_iterator::operator--(int) noexcept {
  const_iterator x(*this);
  --*this;
  return x;
}

template< class Allocator >
inline bool operator==(
  const basic_ustring<Allocator>::const_iterator & lhs,
  const basic_ustring<Allocator>::const_iterator & rhs
) noexcept {
  return lhs.m_iter == rhs.m_iter;
}

template< class Allocator >
inline bool operator!=(
  const basic_ustring<Allocator>::const_iterator & lhs,
  const basic_ustring<Allocator>::const_iterator & rhs
) noexcept {
  return !(lhs == rhs);
}

template< class Allocator >
inline bool operator< (
  const basic_ustring<Allocator>::const_iterator & lhs,
  const basic_ustring<Allocator>::const_iterator & rhs
) noexcept {
  return lhs.m_iter < rhs.m_iter;
}

template< class Allocator >
inline bool operator> (
  const basic_ustring<Allocator>::const_iterator & lhs,
  const basic_ustring<Allocator>::const_iterator & rhs
) noexcept {
  return !(lhs < rhs);
}

template< class Allocator >
inline bool operator<=(
  const basic_ustring<Allocator>::const_iterator & lhs,
  const basic_ustring<Allocator>::const_iterator & rhs
) noexcept {
  return !(lhs > rhs);
}

template< class Allocator >
bool operator>=(
  const basic_ustring<Allocator>::const_iterator & lhs,
  const basic_ustring<Allocator>::const_iterator & rhs
) noexcept {
  return !(lhs < rhs);
}

template< class Allocator >
inline int
basic_ustring<Allocator>::const_iterator::utf8_codepoint(char * out)
  const noexcept
{
  int length = utf::byte_size_from_lead(*m_iter);
  switch (length) {
    case 1:
      out[0] = m_iter[0];
      return 1;
    case 2:
      out[0] = m_iter[0]; out[1] = m_iter[1];
      return 2;
    case 3:
      out[0] = m_iter[0]; out[1] = m_iter[1]; out[2] = m_iter[2];
      return 3;
    case 4:
      out[0] = m_iter[0]; out[1] = m_iter[1];
      out[2] = m_iter[2]; out[3] = m_iter[3];
      return 4;
    default:
      return length;
  }
}

template< class Allocator >
inline std::string
basic_ustring<Allocator>::const_iterator::utf8_codepoint() const {
  return std::string(m_iter, codepoint_size());
}

template< class Allocator >
inline int
basic_ustring<Allocator>::const_iterator::codepoint_size() const noexcept {
  return utf::byte_size_from_lead(*m_iter);
}

inline const char * utf8_data() const noexcept { return m_iter; }


/* **************************************************************************
 * basic_ustring implementation
 * **************************************************************************/

//
// Constructors
//

template< class Allocator >
inline basic_ustring<Allocator>::basic_ustring(
  const basic_ustring & rhs
) {
  assign(rhs);
}

template< class Allocator >
inline basic_ustring<Allocator>::basic_ustring(
  basic_ustring && rhs
) noexcept : m_buffer{ rhs.m_buffer } {}

template< class Allocator >
inline basic_ustring<Allocator>::basic_ustring(
  const const_iterator & first, const const_iterator & last
) {
  assert(first <= last);
  m_data.append(first.m_iter, last.m_iter - m_first.m_iter);
}

//
// Destructor
//

inline ~basic_ustring() {}

//
// Assignment operators
//

template< class Allocator >
inline basic_ustring<Allocator> &
basic_ustring<Allocator>::operator=(const basic_ustring & rhs) {
  if (this != &rhs)
    assign(rhs)
  return *this;
}

template< class Allocator >
inline basic_ustring<Allocator> &
basic_ustring<Allocator>::operator=(basic_ustring && rhs) noexcept {
  if (this != &rhs)
    m_buffer = std::move(rhs.m_buffer);
  return *this;
}

//
// Assigning opertions
//

template< class Allocator >
inline basic_ustring<Allocator> &
basic_ustring<Allocator>::assign(const basic_ustring & rhs) {
  m_data.assign(rhs.m_buffer.m_data, rhs.m_buffer.m_size);
  return *this;
}

template< class Allocator >
inline basic_ustring<Allocator> &
basic_ustring<Allocator>::assign_utf8(const char * utf8_buffer, int size) {
  m_data.assign(utf8_buffer, size);
  if (utf::contains_unsafe(m_buffer.m_data, m_buffer.m_data + size))
    m_buffer.m_size = details::to_safe(m_buffer.m_data, size);
  return *this;
}

template< class Allocator >
inline basic_ustring<Allocator> &
basic_ustring<Allocator>::unchecked_assign_utf8(
  const char * utf8_buffer, int size
) {
  m_data.assign(utf8_buffer, size);
  return *this;
}

//
// Aliasing operations
//

template< class Allocator >
inline basic_ustring<Allocator> &
basic_ustring<Allocator>::alias(const basic_ustring & s) noexcept {
  m_buffer.alias(s.m_buffer.m_data, s.m_buffer.m_size);
  return *this;
}

template< class Allocator >
basic_ustring<Allocator>::basic_ustring &
inline basic_ustring<Allocator>::alias(
  const const_iterator & first, const const_iterator & last
) noexcept {
  assert(first <= last);
  m_buffer.alias(first.data(), last.data() - first.data());
  return *this;
}

template< class Allocator >
inline basic_ustring<Allocator> &
basic_ustring<Allocator>::alias_utf8(
  const char * utf8_buffer, int size
) {
  if (! utf::contains_unsafe(utf8_buffer, utf8_buffer + size)) {
    m_buffer.alias(utf8_buffer, size);
  } else {
    m_buffer.assign(utf8_buffer, size);
    m_buffer.m_size = details::to_safe(m_buffer.m_data, size);
  }
}

template< class Allocator >
inline basic_ustring<Allocator> &
basic_ustring<Allocator>::unchecked_alias_utf8(
  const char * utf8_buffer, int size
) noexcept {
  m_buffer.alias(utf8_buffer, size);
  return *this;
}

//
// Acquiring operations
//

template< class Allocator >
inline basic_ustring<Allocator> &
basic_ustring<Allocator>::acquire_utf8(
  char * utf8_buffer, int size, int capacity
) {
  m_buffer.acquire(utf8_buffer, size, capacity);
  if (utf::contains_unsafe(utf8_buffer, size))
    m_buffer.m_size = details::to_safe(m_buffer.m_data, size);
  return *this;
}

template< class Allocator >
inline basic_ustring<Allocator> &
basic_ustring<Allocator>::acquire_utf8(
  char * utf8_buffer, int size, int capacity
) {
  m_buffer.acquire(utf8_buffer, size, capacity);
  return *this;
}

template< class Allocator >
inline void basic_ustring<Allocator>::clear(bool dealloc) noexcept {
  m_buffer.clear(dealloc);
}

template< class Allocator >
inline bool basic_ustring<Allocator>::empty() const noexcept {
  return m_data.size() == 0;
}

template< class Allocator >
inline int basic_ustring<Allocator>::size() const noexcept {
  return utf::distance(m_buffer.m_data, m_buffer.m_data + m_buffer.m_size);
}
  
template< class Allocator >
inline basic_ustring<Allocator>::const_iterator
basic_ustring<Allocator>::find(
  const basic_ustring & target, const const_iterator pos
) const {
  return const_iterator(utf::find(
    pos.m_iter,
    m_buffer.m_size - (pos.m_iter - m_buffer.m_data),
    rhs.m_buffer.m_data,
    rhs.m_buffer.m_size));
}

template< class Allocator >
inline basic_ustring<Allocator>::const_iterator
basic_ustring<Allocator>::find(const basic_ustring & target) const {
  return const_iterator(utf::find(
    m_buffer.m_data,
    m_buffer.m_size,
    rhs.m_buffer.m_data,
    rhs.m_buffer.m_size));
}
  
  
template< class Allocator >
inline basic_ustring<Allocator>::const_iterator
basic_ustring<Allocator>::find(value_type cp, const const_iterator pos) const {
  return const_iterator(utf::unchecked::find(
    pos.m_iter,
    pos.m_iter + m_buffer.m_size - (pos.m_iter - m_buffer.m_data),
    cp));
}
  
template< class Allocator >
inline const_iterator
basic_ustring<Allocator>::find(value_type cp) const {
  return const_iterator(utf::unchecked::find(
    m_buffer.m_data, m_buffer.m_data + m_buffer.m_size, cp));
}

template< class Allocator >
inline basic_ustring<Allocator>::const_iterator
basic_ustring<Allocator>::rfind(
  const basic_ustring & target, const const_iterator pos
) const {
  return const_iterator(utf::rfind(
    pos.m_iter,
    m_buffer.m_size - (pos.m_iter - m_buffer.m_data),
    rhs.m_buffer.m_data,
    rhs.m_buffer.m_size));
}

template< class Allocator >
inline basic_ustring<Allocator>::const_iterator
basic_ustring<Allocator>::rfind(const basic_ustring & target) const {
  return const_iterator(utf::rfind(
    m_buffer.m_data,
    m_buffer.m_size,
    rhs.m_buffer.m_data,
    rhs.m_buffer.m_size));
}

template< class Allocator >
inline basic_ustring<Allocator>::const_iterator
basic_ustring<Allocator>::rfind(value_type cp, const const_iterator pos) const {
  return const_iterator(utf::unchecked::rfind(
    pos.m_iter,
    pos.m_iter + m_buffer.m_size - (pos.m_iter - m_buffer.m_data),
    cp));
}

template< class Allocator >
inline basic_ustring<Allocator>::const_iterator
basic_ustring<Allocator>::find(value_type cp) const {
  return const_iterator(utf::unchecked::rfind(
    m_buffer.m_data, m_buffer.m_data + m_buffer.m_size, cp));
}
  
template< class Allocator >
inline const char* basic_ustring<Allocator>::utf8_data() const noexcept {
  return m_data.m_data;
}

template< class Allocator >
inline int basic_ustring<Allocator>::utf8_size() const noexcept {
  return m_data.m_size;
}

template< class Allocator >
inline int basic_ustring<Allocator>::utf8_capacity() const noexcept {
  return m_data.m_capacity;
}

template< class Allocator >
inline void basic_ustring<Allocator>::push_back(value_type cp) {
  
  if (utf::is_valid(cp) && utf::is_safe(cp)) {
    m_buffer.reserve(m_buffer.m_size + utf::max_bytes);
    to_bytes(cp, m_buffer.m_data + m_size);
  } else {
    m_buffer.append(" ", 1);
  }
}

template< class Allocator >
template< class InputIt, bool contiguous >
inline basic_ustring<Allocator> &
basic_ustring<Allocator>::append(InputIt first, const InputIt last) {
  assert(first <= last);
  
  if (contiguous)
    return append(reinterpret_cast<const char*>(&(*first)),
                  std::distance(first, last));
    
  for ( ; first != last; ++first)
    push_back(*first);
  return *this;
}

template< class Allocator >
inline basic_ustring<Allocator>::basic_ustring &
basic_ustring<Allocator>::append(
  const const_iterator & first, const const_iterator & last
) {
  assert(first <= last);
  m_buffer.append(first.m_iter, last.m_iter - first.m_iter);
  return *this;
}

template< class Allocator >
inline inline basic_ustring<Allocator> &
inline basic_ustring<Allocator>::append(const basic_ustring & other) {
  m_buffer.append(other.m_buffer.m_data, other.m_buffer.m_size);
}

template< class Allocator >
inline basic_ustring<Allocator> &
basic_ustring<Allocator>::append_utf8(const char * utf8_buffer, int size) {
  m_buffer.append(utf8_buffer, size);
  return *this;
}

template< class Allocator >
template< class ByteOutputIt >
inline ByteOutputIt basic_ustring<Allocator>::to_utf8(
  const const_iterator & first, const const_iterator & last, ByteOutputIt out
) {
  assert(first <= last);
  for (auto it = first; it != last; ++first)
    *out++ = *first;
  return out;
}

template< class Allocator >
inline std::string basic_ustring<Allocator>::to_utf8(
  const const_iterator & first, const const_iterator & last
) {
  assert(first <= last);
  return std::string(first.m_iter, last.m_iter - first.m_iter);
}

template< class Allocator >
inline char* basic_ustring<Allocator>::to_utf8(
  const const_iterator & first, const const_iterator & last, char * out
) {
  assert(first <= last);
  memcpy(out, first.m_iter, last.m_iter - first.m_iter);
  return out + (last.m_iter - first.m_iter);
}
  
template< class Allocator >
inline basic_ustring<Allocator>::const_iterator
basic_ustring<Allocator>::make_iterator(const char * ptr) {
  assert(ptr != nullptr); // TODO: use not_null
  const char * begin = utf8_data();
  const char * end = start + utf8_size();
  assert(ptr >= begin);
  assert(ptr <= end);
  assert(!utf::is_trail(ptr));
  return const_iterator(ptr);
}
  
# if defined(DASKY_USE_FMTLIB) && DASKY_USE_FMTLIB == 1
template< class Allocator >
inline std::string basic_ustring<Allocator>::to_string() const {
  std::string s;
  auto it = begin();
  auto last = end();
  if (it != last) {
    s += "0x{:04x}"_format(*it);
    ++it;
  }
  for (; it != last; ++it)
    s += " 0x{:04x}"
  return s;
}
# else
template< class Allocator >
inline std::string basic_ustring<Allocator>::to_string() const {
  std::ostringstream ss;
  auto it = begin();
  auto last = end();
  ss << std::setw(4) << std::hex << std::showbase;
  if (it != last) {
    ss << *it;
    ++it;
  }
  for (; it != last; ++it)
    ss << ' ' << *it;
  return ss.str();
}
# endif

using ustring = basic_ustring< std::allocator<char> >;
  
inline make_ustring(char * utf8_buffer, int size, int capacity) {
  return ustring().acquire(utf8_buffer, size, capacity);
}
  
inline make_ustring_alias(char * utf8_buffer, int size) {
  return ustring().alias(utf8_buffer, size);
}
  
inline utf8_to_ustring(const char * utf8_buffer, int size, bool copy) {
  ustring s;
  return copy ? s.assign(utf8_buffer, size) : s.alias(utf8_buffer, size);
}
  
template< class T >
inline utf8_to_ustring(const T & s, bool copy) {
  return utf8_to_ustring(s.data(), s.size(), copy);
}
  
template< class T >
inline utf8_to_ustring(const T & s) {
  return utf8_to_ustring(s.data(), s.size(), true);
}
  
inline utf8_to_ustring(const char * s, int size, bool copy) {
  return utf8_to_ustring(s, size, copy);
}
  
inline utf8_to_ustring(const char * s, int size) {
  return utf8_to_ustring(s, size, true);
}

END_DASKY_NAMESPACE

#endif /* dasky_ustring_h__ */
