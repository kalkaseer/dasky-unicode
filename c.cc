//
//  c.c
//  dasky-encoding
//
//  Created by Kareem Alkaseer on 03/12/2017.
//
//

#include "dasky/utf/c.h"
#include "dasky/utf/codec.h"
#include "dasky/utf/predicate.h"

#if !defined(DASKY_OUT_OF_TREE_BUILD)
using namespace dasky;
using namespace dasky::utf;
#endif

/// Return true if the byte buffer specified could possibly be decoded by
/// `to_rune`, false otherwise. Regardless of the return value, this function
/// only checks the possibility of decoding, not whether the buffer contains
/// a valid UTF-8 sequence. This function could be used by client code
/// that receive input one byte at a time and need to to recognise when a full
/// rune has arrived.
bool u8_rune_is_complete(const char * c, size_t n) {
  return utf::is_complete(c, n);
}
  
/// Return the number of complete runes contained in the first `n` bytes of
/// the specified byte buffer.
/// *Requires* that `s` is null-terminated.
size_t u8_runes_nsize(const char * s, size_t n) {
  return utf::runes_size(s, n);
}

/// Return the number of complete runes contained in the specified byte buffer.
/// *Requires* that `s` is null-terminated.
size_t u8_runes_size(const char * s) {
  return utf::runes_size(s);
}

/// Return the length of a null-terminated rune buffer.
size_t u8_runes_size(const u8_rune_t * s) {
  return utf::runes_size(s);
}
  
/// Return the number of bytes required to encode a sequence of runes into
/// a UTF-8 sequence.
size_t u8_runes_decoded_byte_size(const u8_rune_t * r, size_t n) {
  return utf::decoded_byte_size(r, n);
}

/// Return the number of bytes required to encode a rune `r` into a UTF-8
/// sequence.
size_t u8_rune_byte_size(u8_rune_t r) {
  return utf::byte_size(r);
}

/// Encodes one rune as at most rune_max_bytes into the specified buffer.
/// @return the number of bytes used for encoding the rune.
/// @requires buf is big enough to contain at most rune_max_bytes (4 bytes).
int u8_rune_to_bytes(u8_rune_t r, char * buf) {
  return utf::to_bytes(r, buf);
}

/// Decodes at most rune_max_bytes bytes from the specified buffer - which
/// must be null-terminated - into one rune.
/// Use this version if you are sure that the buffer is null-terminated
/// @return number of bytes consumed from the buffer; on success `r` is
/// is set to the decoded rune, on failure `r` is set to `rune_error`.
int u8_byte_to_rune(const char * buf, u8_rune_t * r) {
  return utf::next(buf, *r);
}
  
const u8_rune_t * u8_find_rune(const u8_rune_t * s, u8_rune_t c) {
  return utf::find(s, c);
}

const char * u8_find_rune_bytes(const char * s, u8_rune_t c) {
  return utf::find(s, c);
}

const u8_rune_t * u8_rfind_rune(const u8_rune_t * s, u8_rune_t c) {
  return utf::find(s, c);
}

const char * u8_rfind_rune_bytes(const char * s, u8_rune_t c) {
  return utf::find(s, c);
}
  
const u8_rune_t * u8_substring(const u8_rune_t * s1, const u8_rune_t * s2) {
  return utf::find(s1, s2);
}
  
const char * u8_substring_bytes(const char * s1, const char * s2) {
  return utf::find(s1, s2);
}
  
int u8_compare(const u8_rune_t * s1, const u8_rune_t * s2) {
  return utf::compare(s1, s2);
}

int u8_ncompare(const u8_rune_t * s1, const u8_rune_t * s2, size_t n) {
  return utf::compare(s1, s2, n);
}
  
u8_rune_t * u8_copy(const u8_rune_t * src, u8_rune_t * dest) {
  return utf::copy(src, dest);
}

u8_rune_t * u8_ncopy(const u8_rune_t * src, u8_rune_t * dest, size_t n) {
  return utf::copy(src, dest, n);
}

u8_rune_t * u8_runes_ecopy(
  const u8_rune_t * src, u8_rune_t * e, u8_rune_t * dest
) {
  return utf::ecopy(src, e, dest);
}

char * u8_bytes_ecopy(const char * src, char * e, char * dest) {
  return utf::ecopy(src, e, dest);
}

u8_rune_t * concat(u8_rune_t * s1, const u8_rune_t * s2) {
  return utf::concat(s1, s2);
}

u8_rune_t * nconcat(u8_rune_t * s1, const u8_rune_t * s2, size_t n) {
  return utf::concat(s1, s2, n);
}

# if !defined(DASKY_OMIT_UTF_PREDICATES) || DASKY_OMIT_UTF_PREDICATES != 1

u8_rune_t u8_toupper(u8_rune_t r) {
  return utf::upper(r);
}

u8_rune_t u8_tolower(u8_rune_t r) {
  return utf::lower(r);
}

u8_rune_t u8_totitle(u8_rune_t r) {
  return utf::title(r);
}

bool u8_isupper(u8_rune_t r) {
  return utf::is_upper(r);
}

bool u8_islower(u8_rune_t r) {
  return utf::is_lower(r);
}

bool u8_istitle(u8_rune_t r) {
  return utf::is_title(r);
}

bool u8_isalpha(u8_rune_t r) {
  return utf::is_alpha(r);
}

bool u8_isdigit(u8_rune_t r) {
  return utf::is_digit(r);
}

bool u8_isideographic(u8_rune_t r) {
  return utf::is_ideographic(r);
}

bool u8_isspace(u8_rune_t r) {
  return utf::is_space(r);
}

# endif // !defined(DASKY_OMIT_UTF_PREDICATES) || DASKY_OMIT_UTF_PREDICATES != 1
