//
//  tests_main.cc
//  dasky
//
//  Created by Kareem Alkaseer on 06/12/2017.
//
//

#include <chrono>
#include <gtest/gtest.h>

#include "dasky/utf/utf.h"

//static std::string sample_text() {
//  return u8"久有归天愿终过鬼门关千里来寻归宿春华变苍颜到处群魔乱舞更有妖雾盘绕暗道入阴间过了"
//         u8"阎王殿险处不须看风雷动旌旗奋忆人寰八十三年过去弹指一挥间中原千军逐蒋城楼万众检阅"
//         u8"褒贬满载还世上无难事只怕我癫痫";
//}
//
//i64 test1(int loops, dasky::utf::ustring & runes) {
//  
//  std::string str = sample_text();
//  dasky::utf::rune cp;
//  
//  auto t1 = std::chrono::high_resolution_clock::now();
//  
//  for (int i = 0; i < loops; ++i) {
//    runes.clear();
//    for (int k = 0; k < str.size(); ++k) {
//      dasky::utf::to_rune(str.begin()+k, str.end(), cp);
//      runes.append(1, cp);
//    }
//  }
//  
//  auto t2 = std::chrono::high_resolution_clock::now();
//  auto diff = t2 - t1;
//  
//  return std::chrono::duration<double, std::nano>(diff).count();
//}
//
//i64 test2(int loops, dasky::utf::ustring & runes) {
//  
//  std::string str = sample_text();
//  dasky::utf::rune cp;
//  
//  auto t1 = std::chrono::high_resolution_clock::now();
//  
//  for (int i = 0; i < loops; ++i) {
//    runes.clear();
//    for (int k = 0; k < str.size(); ++k) {
//      dasky::utf::next(str.begin()+k, str.end(), cp);
//      runes.append(1, cp);
//    }
//  }
//  
//  auto t2 = std::chrono::high_resolution_clock::now();
//  auto diff = t2 - t1;
//  
//  return std::chrono::duration<double, std::nano>(diff).count();
//}

int main(int argc, char ** argv) {
  
//  int loops = 1'000'000;
//  std::string str;
//  dasky::utf::ustring runes;
//  runes.reserve(100);
//  auto r1 = test1(loops, runes);
//  auto copy = runes;
//  auto r2 = test2(loops, runes);
//  std::cout << "to_rune 1: " << r1 << " ns" << std::endl;
//  std::cout << "to_rune 2: " << r2 << " ns" << std::endl;
//  std::cout << "diff: " << r2 - r1 << " ns" << std::endl;
//  assert(runes == copy);
  
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
