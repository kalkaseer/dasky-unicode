/*
 * base.h
 *
 *  Created on: Aug 4, 2016
 *      Author: Kareem Alkaseer
 */

#ifndef DASKY_UTF_BASE_H_
#define DASKY_UTF_BASE_H_

#include <string>
#include <stdexcept>
#include "dasky/utf/config.h"
#include "dasky/bits.h"

BEGIN_DASKY_NAMESPACE
namespace utf {

/// Type to represent runes: the Unicode codepoint, i.e., the equivalent to
/// a character in ASCII.
using rune = char32_t;
  
/// Type to represent UTF-16 code units. A code point is represented as
/// either on or two code units.
using ch16 = char16_t;
  
/// Type to represent UTF-32 code units.A code point is one-to-one mapping to
/// a code unit.
using ch32 = char32_t;
  
/// Type representing a Unicode string.
using ustring = std::u32string;
  
/// The char sequence produced by encoding an error rune. Can be used to check
/// for a sequence representing a decoding error in a buffer.
constexpr const char error_chars[] = {
  char(0xef), char(0xbf), char(0xbd)
};
  
/// The byte sequence produced by encoding an error rune. Can be used to check
/// for a sequence representing a decoding error in a buffer.
constexpr const u8 error_bytes[] = { 0xef, 0xbf, 0xbd };
  
/// Byte length of the error sequence.
constexpr std::size_t error_bytes_size =
  sizeof(error_chars) / sizeof(error_chars[0]);
  
/* ********************************************************************
 * Internals
 * ********************************************************************/

// Do not use anything from the details namespace.
namespace details {
  
  namespace mpl {
    template< class T, class... Args >
    struct contains;
      
    template< class T >
    struct contains<T> : std::false_type {};
      
    template< class T, class... Args >
    struct contains<T, T, Args...> : std::true_type {};
      
    template< class T, class U, class... Args >
    struct contains<T, U, Args...> : contains<T, Args...> {};
      
    template< class T, class... Args >
    constexpr bool contains_v = contains<T, Args...>::value;
  } /* namespace mpl */
  
  /// SFINAE trait class that by default defines its `value` to true if
  /// dereferencing type `T` produces a `u8` value, false otherwise. Needs
  /// specialisations for type not supporting dereferencing.
  /// @INTERNAL.
  template< class T >
  struct has_u8_underlying_value_type {
    
    template< class U >
    static auto test(U&& u)
      -> decltype(
          std::is_same<decltype(*std::declval<U>()), u8>::value, char(0)) {}

    static long test(...) {}

    static const bool value = (sizeof(test(std::declval<T&&>())) == 1);
  };
  
  // `has_u8_underlying_value_type` specialisation for integral types.
  
  template<>
  struct has_u8_underlying_value_type<u8> : public std::true_type {};
  
  template<>
  struct has_u8_underlying_value_type<i8> : public std::false_type {};
  
  template<>
  struct has_u8_underlying_value_type<u16> : public std::false_type {};
  
  template<>
  struct has_u8_underlying_value_type<i16> : public std::false_type {};
  
  template<>
  struct has_u8_underlying_value_type<u32> : public std::false_type {};
  
  template<>
  struct has_u8_underlying_value_type<i32> : public std::false_type {};
  
  template<>
  struct has_u8_underlying_value_type<u64> : public std::false_type {};
  
  template<>
  struct has_u8_underlying_value_type<i64> : public std::false_type {};
  
  # if defined(DASKY_HAS_INT128) && DASKY_HAS_INT128 == 1
  template<>
  struct has_u8_underlying_value_type<u128> : public std::false_type {};
  template<>
  struct has_u8_underlying_value_type<i128> : public std::false_type {};
  # endif
  
  /// Enable-if class that enables retrieving a value of type `T` without
  /// the need to mask it first.
  /// @INTERNAL.
  template< class T >
  using enable_unmasked_byte = typename std::enable_if<
      has_u8_underlying_value_type<typename std::decay<T>::type>::value,
      T
    >::type;
  
  /// Enable-if class that enables retrieving a value of type `T` with
  /// the need to mask it first.
  /// @INTERNAL.
  template< class T >
  using disable_unmasked_byte = typename std::enable_if<
      !has_u8_underlying_value_type<typename std::decay<T>::type>::value,
      T
    >::type;
  
  /// Get the current byte from an iterator. This function makes sure we get
  /// an unsigned byte.
  /// @INTERNAL.
  template< class ByteIter, enable_unmasked_byte<ByteIter>* = nullptr >
  opt_always_inline
  constexpr u8 ensure_byte(ByteIter & it) {
    return *it;
  }
  
  /// Get the current byte from an iterator. Enabled for types requiring
  /// masking the value.
  /// @INTERNAL.
  template< class ByteIter, disable_unmasked_byte<ByteIter>* = nullptr >
  opt_always_inline
  constexpr u8 ensure_byte(ByteIter & it) {
    return mask8(*it);
  }
  
  /// Get the current byte from an iterator. This function makes sure we get
  /// an unsigned byte. This version is useful for pointers.
  /// @INTERNAL.
  template< class ByteIter, enable_unmasked_byte<ByteIter>* = nullptr >
  opt_always_inline
  constexpr u8 ensure_byte(ByteIter && it) {
    return *it;
  }
  
  template< class ByteIter, disable_unmasked_byte<ByteIter>* = nullptr >
  opt_always_inline
  constexpr u8 ensure_byte(ByteIter && it) {
    return mask8(*it);
  }
  
  
  constexpr ch16 lead_surrogate_min  { 0xd800 };
  constexpr ch16 lead_surrogate_max  { 0xdbff };
  constexpr ch16 trail_surrogate_min { 0xdc00 };
  constexpr ch16 trail_surrogate_max { 0xdfff };
  constexpr ch16 lead_offset      {
    lead_surrogate_min - (0x10000 >> 10) };
  constexpr std::make_unsigned<ch32>::type surrogate_offset {
    0x10000u - (lead_surrogate_min << 10) - trail_surrogate_min };

  /// Returns true if a code unit is a lead surrogate unit.
  /// @INTERNAL
  template< class T >
  opt_always_inline
  constexpr bool is_lead_surrogate(T cp) {
    return (cp >= lead_surrogate_min && cp <= lead_surrogate_max);
  }

  /// Returns true if a code unit is a trail surrogate unit.
  /// @INTERNAL
  template< class T >
  opt_always_inline
  constexpr bool is_trail_surrogate(T cp) {
    return (cp >= trail_surrogate_min && cp <= trail_surrogate_max);
  }

  /// Returns true if a code unit is a surrogate unit.
  /// @INTERNAL
  template< class T >
  opt_always_inline
  constexpr bool is_surrogate(T cp) {
    return (cp >= lead_surrogate_min && cp <= trail_surrogate_max);
  }
  
  /// Traits for runes.
  /// @INTERNAL
  enum class rune_traits : i32 {
    sync       = 0x80,      //< cannot be a part of a UTF sequence
    same       = 0x80,      //< rune and UTF sequence are the same
    error      = 0xfffd,    //< UTF decoding error
    max        = 0x10ffff,  //< maximum rune value
  };

  /// Casts a `rune_trait` to its integral value.
  /// @INTERNAL
  constexpr i32 integer(rune_traits t) noexcept {
    return static_cast<i32>(t);
  }
  
  /// State variables for processing UTF8 sequences.
  /// @INTERNAL
  enum class state : u32 {
    b1 = 7,
    bx = 6,
    b2 = 5,
    b3 = 4,
    b4 = 3,
    b5 = 2,

    t1 = ((1 << (b1 + 1)) - 1) ^ 0xff, // 0000 0000
    tx = ((1 << (bx + 1)) - 1) ^ 0xff, // 1000 0000   0x80
    t2 = ((1 << (b2 + 1)) - 1) ^ 0xff, // 1100 0000
    t3 = ((1 << (b3 + 1)) - 1) ^ 0xff, // 1110 0000
    t4 = ((1 << (b4 + 1)) - 1) ^ 0xff, // 1111 0000
    t5 = ((1 << (b5 + 1)) - 1) ^ 0xff, // 1111 1000

    r1 = (1 << (b1 + 0 * bx)) - 1,     // 0000 0000 0111 1111  0x7f
    r2 = (1 << (b2 + 1 * bx)) - 1,     // 0000 0111 1111 1111  0x7ff
    r3 = (1 << (b2 + 2 * bx)) - 1,     // 1111 1111 1111 1111  0xf7ff
    r4 = (1 << (b2 + 3 * bx)) - 1,     // 0001 1111 1111 1111 1111 1111 0x1ff7ff
    
    mask12 = 0xfff,    //             1111 1111 1111
    mask16 = 0xffff,   //        1111 1111 1111 1111
    mask18 = 0x3ffff,  //     11 1111 1111 1111 1111
    mask21 = 0x1fffff, // 1 1111 1111 1111 1111 1111

    maskx = (1 << bx) - 1,             // 0011 1111
    testx = maskx ^ 0xff,              // 1100 0000
  };
    
  /// Casts a `state` variable to its integral value.
  /// @INTERNAL
  constexpr u32 integer(state x) noexcept {
    return static_cast<u32>(x);
  }
    
  // Comparison operators. @INTERNAL
    
  constexpr bool operator==(i32 n, state x) noexcept {
    return n == integer(x);
  }
    
  constexpr bool operator==(state x, i32 n) noexcept {
    return n == integer(x);
  }
    
  constexpr bool operator!=(i32 n, state x) noexcept {
    return n != integer(x);
  }
    
  constexpr bool operator!=(state x, i32 n) noexcept {
    return n != integer(x);
  }
    
  constexpr bool operator< (i32 n, state x) noexcept {
    return n < integer(x);
  }
    
  constexpr bool operator< (state x, i32 n) noexcept {
    return integer(x) < n;
  }
    
  constexpr bool operator> (i32 n, state x) noexcept {
    return n > integer(x);
  }
    
  constexpr bool operator> (state x, i32 n) noexcept {
    return integer(x) > n;
  }
    
  constexpr bool operator<= (i32 n, state x) noexcept {
    return n <= integer(x);
  }
    
  constexpr bool operator<= (state x, i32 n) noexcept {
    return integer(x) <= n;
  }
    
  constexpr bool operator>= (i32 n, state x) noexcept {
    return n >= integer(x);
  }
    
  constexpr bool operator>= (state x, i32 n) noexcept {
    return integer(x) >= n;
  }
    
  // Bitwise operators
    
  constexpr i32 operator& (i32 n, state x) noexcept {
    return n & integer(x);
  }
    
  constexpr i32 operator& (state x, i32 n) noexcept {
    return integer(x) & n;
  }
    
  constexpr i32 operator| (i32 n, state x) noexcept {
    return n | integer(x);
  }
    
  constexpr i32 operator| (state x, i32 n) noexcept {
    return integer(x) | n;
  }
    
  constexpr i32 operator^ (i32 n, state x) noexcept {
    return n ^ integer(x);
  }
    
  constexpr i32 operator^ (state x, i32 n) noexcept {
    return n ^ integer(x);
  }
    
  constexpr i32 & operator&= (i32 & n, state x) noexcept {
    n &= integer(x);
    return n;
  }
    
  constexpr i32 & operator&= (state x, i32 & n) noexcept {
    n &= integer(x);
    return n;
  }
    
  constexpr i32 & operator|= (i32 & n, state x) noexcept {
    n |= integer(x);
    return n;
  }
    
  constexpr i32 & operator|= (state x, i32 & n) noexcept {
    n |= integer(x);
    return n;
  }
    
  constexpr i32 & operator^= (i32 & n, state x) noexcept {
    n ^= integer(x);
    return n;
  }
    
  constexpr i32 & operator^= (state x, i32 & n) noexcept {
    n ^= integer(x);
    return n;
  }
    
  constexpr i32 operator<< (i32 n, state x) noexcept {
    return n << integer(x);
  }
    
  constexpr i32 operator>> (i32 n, state x) noexcept {
    return n >> integer(x);
  }
    
  // Math operators
    
  constexpr i32 operator* (i32 n, state x) noexcept {
    return n * integer(x);
  }
    
  constexpr i32 operator* (state x, i32 n) noexcept {
    return n * integer(x);
  }
    
} /* namespace details */

/* ********************************************************************
 * Exceptions (omitted if DASKY_NO_EXCEPTIONS == 1
 * ********************************************************************/
  
# if !defined(DASKY_NO_EXCEPTION) || DASKY_NO_EXCEPTIONS != 1

/// Base class for exceptions thrown by the UTF module.
class exception : public std::exception {};
  
/// Thrown when an invalid encoding code units are encountered.
class bad_encoding : public exception {
public:
  bad_encoding(i32 blame) : m_blame{ blame } {}
  virtual const char * what() const throw() override{ return "bad encoding"; }
  constexpr i32 blame() const { return m_blame; }
private:
  i32 m_blame;
};
  
/// Thrown when an invalid codepoint is encountered.
class bad_codepoint : public exception {
public:
  bad_codepoint(rune cp) : m_cp{ cp } {}
  virtual const char * what() const throw() override{ return "bad codepoint"; }
  constexpr rune codepoint() const { return m_cp; }
private:
  rune m_cp;
};
  
/// Thrown when more memory is required, e.g., buffer not large enough.
class memory_error : public exception {
public:
  virtual const char * what() const throw() override { return "memory error"; }
};
  
# endif // !defined(DASKY_NO_EXCEPTION) || DASKY_NO_EXCEPTIONS != 1
    
/* ********************************************************************
 * Error handling policies.
 * ********************************************************************/
  
/// Indicates that when an error is encountered it should be amended and
/// ignored if possible. For example, when decoding an error sequence may
/// be replaced by `error_rune`.
class ignore_error_policy_tag {};
    
# if !defined(DASKY_NO_EXCEPTION) || DASKY_NO_EXCEPTIONS != 1
/// Tag to indicate that on error an exception should be thrown. For example,
/// when decoding, an error sequence will result in `bad_encoding` being
/// thrown.
/// *Only* available when `DASKY_NO_EXCEPTIONS` is either not defined
/// or is not equal to `1`.
class throw_on_error_policy_tag {};
# endif // !defined(DASKY_NO_EXCEPTION) || DASKY_NO_EXCEPTIONS != 1
    
/// The library's default error handling policy tag.
using default_error_policy_tag = ignore_error_policy_tag;
    
namespace details {
  
  // Policy to handle errors encountered during decoding.
  template< class Tag >
  struct decoding_error_policy {};
  
  # if !defined(DASKY_NO_EXCEPTION) || DASKY_NO_EXCEPTIONS != 1
  // Throw exceptions to handle errors encountered during decoding.
  template<>
  struct decoding_error_policy<throw_on_error_policy_tag> {
    
    opt_always_inline opt_noreturn static void execute() {
      throw bad_encoding(0);
    }
    
    opt_always_inline opt_noreturn static void execute(i32 cp) {
      throw bad_encoding(cp);
    }
    
    template< class Iter >
    opt_always_inline
    static void execute(Iter & first, Iter & last, int bytes) {
      throw bad_encoding(*first);
    }
  };
  # endif // !defined(DASKY_NO_EXCEPTION) || DASKY_NO_EXCEPTIONS != 1
  
  // Replace errors encountered during decoding and continue.
  template<>
  struct decoding_error_policy<ignore_error_policy_tag> {
    
    constexpr static void execute() {}
    
    constexpr static void execute(i32 cp) {}
    
    template< class Iter >
    opt_always_inline
    static void execute(Iter & first, Iter & last, int bytes) {}
  };
  
  // Policy to handle errors due to lack of memory, e.g., shorter buffer
  // lengths.
  template< class Tag >
  struct memory_error_policy {};
  
  # if !defined(DASKY_NO_EXCEPTION) || DASKY_NO_EXCEPTIONS != 1
  // Throw exceptions to handle memory errors.
  template<>
  struct memory_error_policy<throw_on_error_policy_tag> {
    opt_always_inline static void execute() { throw memory_error(); }
  };
  # endif // !defined(DASKY_NO_EXCEPTION) || DASKY_NO_EXCEPTIONS != 1
  
  // Return on memory error.
  template<>
  struct memory_error_policy<ignore_error_policy_tag> {
    constexpr static void execute() {}
  };
} /* namespace details */

  
/// A codepoint indicating a decoding error.
constexpr rune error_rune{ integer(details::rune_traits::error) };
    
/// Maximum number of bytes to encode a codepoint.
constexpr i32 max_bytes{ 4 };
  
/// Byte order mark
constexpr uint8_t bom[] = { 0xef, 0xbb, 0xbf };
    
/// Return true if the current sequence is a BOM.
template< class ByteIter >
bool is_bom(ByteIter first, ByteIter last) {
  return
    ((first != last) && (mask8(details::ensure_byte(first++)) == bom[0])) &&
    ((first != last) && (mask8(details::ensure_byte(first++)) == bom[1])) &&
    ((first != last) && (mask8(details::ensure_byte(first))   == bom[2]));
}
    
/// Return true if `cp` is a valid codepoint, false otherwise. A valid
/// codepoint belongs to one of the following ranges [0, 0xD800) or
/// [0xE000, 0x10FFFF]. Surrogate points are invalid.
constexpr bool is_valid(rune cp) noexcept {
  using namespace details;
  return (cp < 0xD800) || (cp >=0xE000 && cp <= integer(rune_traits::max));
}
    
///// Return true if byte `x` is a valid trailing UTF-8 byte, i.e.,
///// [0x80, 0xBF].
//template< class T >
//constexpr bool is_trail(T x) noexcept {
//  return static_cast<signed char>(static_cast<char>(x)) < -0x40;
//}

  /// Returns true if `x` could be a trailing byte.
  /// Enabled for types that do not require masking.
  template< class T, details::enable_unmasked_byte<T>* = nullptr >
  opt_always_inline
  constexpr bool is_trail(T x) {
    return ((x >> 6) == 0x2);
  }
  
  /// Returns true if `x` could be a trailing byte.
  /// Enabled for types that require masking.
  /// @INTERNAL
  template< class T, details::disable_unmasked_byte<T>* = nullptr >
  constexpr bool is_trail(T x) {
    return ((mask8(x) >> 6) == 0x2);
  }
    
/// Return true for codepoints that considered safe by the library. Most
/// importantly, these codepoints are considered to be saved to be included
/// in resources on the Web.
/// The following codepoints are considered unsafe:
///   Control codepoints:
///       [U+0000, U+001F], [U+007F, U+009F], except whitespace (U+0020)
///   Surrogate codepoints:
///       [U+D800, U+DFFF]
///   Non-charachter codepoints:
///       [U+FDD0, U+FDEF], [U+**FFFE to U+**FFFF]
constexpr bool is_safe(rune c) noexcept {
  return !((c <= 0x08)                  ||
           (c == 0x0B)                  ||
           (c == 0x0C)                  ||
           (c >= 0x0E && c <= 0x1F)     ||
           (c >= 0x7F && c <= 0x9F)     ||
           (c >= 0xD800 && c <= 0xDFFF) ||
           (c >= 0xFDD0 && c <= 0xFDEF) ||
           (c & 0xFFFE) == 0xFFFE);
}

} // namespace utf
END_DASKY_NAMESPACE




#endif /* DASKY_UTF_BASE_H_ */
