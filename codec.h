/*
 * codec.h
 *
 *  Created on: Aug 4, 2016
 *      Author: Kareem Alkaseer
 */

#ifndef DASKY_UTF_CODEC_H_
#define DASKY_UTF_CODEC_H_

#include <cstddef>  // size_t
#include "dasky/utf/config.h"
#include "dasky/utf/base.h"
#include "dasky/iterator.h"
#include "dasky/bits.h"

BEGIN_DASKY_NAMESPACE
namespace utf {
  
namespace details {
  

// Type that represents thw underlying type `T` difference_type. Mostly
// meaningful for iterators.
template< class T >
using difference_type = typename std::iterator_traits<T>::difference_type;

// Encode a 1-byte codepoint into an iterator and increment the iterator.
// This function is only enabled if the iterator does not support operator[],
// e.g., the iterator is not a pointer.
template< class OutputByteIt >
opt_always_inline
constexpr void encode_one_byte(OutputByteIt & it, u32 cp) {
  *it = cp;
  ++it;
}

// Encode a 2-byte codepoint into an iterator and increment the iterator.
// This function is only enabled if the iterator does not support operator[],
// e.g., the iterator is not a pointer.
template< class OutputByteIt >
opt_always_inline
constexpr void encode_two_byte(OutputByteIt & it, u32 cp) {
  *it = state::t2 | (cp >> 1 * state::bx); ++it;
  *it = state::tx | (cp & state::maskx);   ++it;
}
 
// Encode a 3-byte codepoint into an iterator and increment the iterator.
// This function is only enabled if the iterator does not support operator[],
// e.g., the iterator is not a pointer.
template< class OutputByteIt >
opt_always_inline
constexpr void encode_three_byte(OutputByteIt & it, u32 cp) {
  *it = state::t3 | ( cp >> 2 * state::bx);                 ++it;
  *it = state::tx | ((cp >> 1 * state::bx) & state::maskx); ++it;
  *it = state::tx | ( cp & state::maskx);                   ++it;
}

// Encode a 4-byte codepoint into an iterator and increment the iterator.
// This function is only enabled if the iterator does not support operator[],
// e.g., the iterator is not a pointer.
template< class OutputByteIt >
opt_always_inline
constexpr void encode_four_byte(OutputByteIt & it, u32 cp) {
  *it = state::t4 | ( cp >> 3 * state::bx);                 ++it;
  *it = state::tx | ((cp >> 2 * state::bx) & state::maskx); ++it;
  *it = state::tx | ((cp >> 1 * state::bx) & state::maskx); ++it;
  *it = state::tx | ( cp & state::maskx);                   ++it;
}
  
# if !defined(DASKY_OPTIMIZE_FOR_SIZE) || DASKY_OPTIMIZE_FOR_SIZE != 1
struct decoder_state { i32 c1, c2, c3; };
# else
struct decoder_state { u8 c1, c2, c3; };
# endif
  
  constexpr const char byte_size_lead_table[256] = {
//  0 1 2 3 4 5 6 7 8 9 a b c d e f
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // 0x00
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // 0x10
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // 0x20
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // 0x30
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // 0x40
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // 0x50
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // 0x60
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // 0x70 End ASCII
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // 0x80 0x80 to 0xc1 invalid
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // 0x90
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // 0xa0
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // 0xb0
    1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2, // 0xc0 0xc2 to 0xdf 2 byte
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, // 0xd0
    3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3, // 0xe0 0xe0 to 0xef 3 byte
    4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0  // 0xf0 0xf0 to 0xf4 4 byte,
                                     //      0xf5 >= invalid
  };
  
} /* namespace details */
 
/// Returns the byte-length expected for the codepoint whose lead (first)
/// byte is `lead`.
template< class ByteType >
opt_always_inline
constexpr int byte_size_from_lead(ByteType lead) noexcept {
//  if       (lead < 0x80)        return 1;
//  else if ((lead >> 5) == 0x6)  return 2;
//  else if ((lead >> 4) == 0xe)  return 3;
//  else if ((lead >> 3) == 0x1e) return 4;
//  else return 0;
  return details::byte_size_lead_table[static_cast<u8>(lead)];
}

/// Encodes a single rune to UTF-8 byte sequence and return the number of
/// bytes encoded.The iterator `it` will point to the byte past the encoded
/// sequence, i.e., a subsequent call to this function with the same iterator
/// does not need to advance the iterator.
/// @param r Unicode rune to encode.
/// @param an output iterator to write byte sequence to.
/// @tparam OutputByteIter a byte `OutputIterator`.
/// @return number of bytes used to encode the rune.
template< class OutputByteIter >
int to_bytes(rune r, OutputByteIter & it) {
  
  using namespace details;
  
  // change to unsigned for range check with enum values
  u32 ur = r;

  if (ur <= 0x007F) {
//  if (ur <= state::r1) {
    details::encode_one_byte(it, ur);
    return 1;
  }

  if (ur <= 0x07FF) {
//  if (ur <= state::r2) {
    details::encode_two_byte(it, ur);
    return 2;
  }

  // an out-of-range rune cannot be smaller than three bytes
  // and rune_traits::error is encoded in three bytes
  if (ur > integer(rune_traits::max))
    ur = integer(rune_traits::error);

  if (ur <= 0xFFFF) {
//  if (ur <= state::r3) {
    details::encode_three_byte(it, ur);
    return 3;
  }

  // The codepoint is assumed to be valid, no range check <= 0x10FFFF
  details::encode_four_byte(it, ur);
  return 4;
}
  
/// @related `to_bytes(rune r, OutputByteIter & it)`.
template< class OutputByteIter >
inline int to_bytes(rune r, OutputByteIter && it) {
  return to_bytes(r, it);
}
  
# define advance_or_return(ret)                               \
  if (++(first) == (last)) {                                  \
    details::memory_error_policy<ErrorPolicy>::execute();     \
    cp = replacement;                                         \
    return ret;                                               \
  }

/// Validates and decodes the next rune from a byte sequence. The function
/// advances the `first` iterator by the number of bytes consumed by the call.
/// On return, `first` will point to the first unconsumed byte.
/// If you want to ignore a decoding erorr, rewind `first` by "the number of
/// bytes consumed + 1" and carry on decoding from the next byte.
///
/// @param it an input byte iterator for the byte sequence.
/// @param cp an output parameter to write the decoded rune to.
/// @tparam replacement The rune to use to report decoding error.
/// @tparam ErrorPolicy Error handling policy tag.
/// @tparam ByteIter A byte `InputIterator`.
///
/// @return On success, the number of bytes used to encode the rune together
/// with setting `cp` to the decoded rune, on failure, the negaitve value of
/// the number of bytes consumed.
/// sets `cp` to `error_rune`.
///
/// @requires `first` must be at least dereferenceable for one time.
/// @requires `first < last`
template< rune replacement = error_rune,
          class ErrorPolicy = default_error_policy_tag,
          class ByteIter >
int next(ByteIter & first, ByteIter & last, rune & cp) {
  
  using namespace details;

  cp = details::ensure_byte(first);
  i32 c1, c2, c3;
  
  // 1-byte sequence: 00 - 7f (t1)
  if (cp < state::tx) {
    ++first;
    return 1;
  }

  advance_or_return(-1);
  
  // 2-byte: 0080 - 07ff (t2 tx)
  c1 = ensure_byte(first) ^ state::tx;

  if (c1 & state::testx) {
    decoding_error_policy<ErrorPolicy>::execute();
    cp = replacement;
    return -1;
  }

  if (cp < state::t3) {
    
    if (cp < state::t2) {
      decoding_error_policy<ErrorPolicy>::execute();
      cp = replacement;
      return -1;
    }
    
    cp = ((cp << state::bx) | c1) & state::r2;
    if (cp <= state::r1) {
      decoding_error_policy<ErrorPolicy>::execute();
      cp = replacement;
      return -1;
    }
    
    ++first;
    return 2;
  }
  
  advance_or_return(-2);

  // 3-byte: 0800 - ffff (t3 tx tx)
  c2 = details::ensure_byte(first) ^ state::tx;

  if (c2 & state::testx) {
    decoding_error_policy<ErrorPolicy>::execute();
    cp = replacement;
    return -2;
  }

  if (cp < state::t4) {
    cp = ((((cp << state::bx) | c1) << state::bx) | c2) & state::r3;
    if (cp <= state::r2) {
      decoding_error_policy<ErrorPolicy>::execute();
      cp = replacement;
      return -2;
    }
    ++first;
    return 3;
  }
  
  advance_or_return(-3);

  // 4-byte: 010000 - 1fffff (t4 tx tx tx)
  c3 = details::ensure_byte(first) ^ state::tx;

  if (c3 & state::testx) {
    decoding_error_policy<ErrorPolicy>::execute();
    cp = replacement;
    return -3;
  }

  if (cp < state::t5) {
    cp = ((((((cp << state::bx) | c1) <<
              state::bx) | c2) << state::bx) | c3) & state::r4;
    if (cp <= state::r3 || cp > integer(rune_traits::max)) {
      decoding_error_policy<ErrorPolicy>::execute();
      cp = replacement;
      return -3;
    }
    ++first;
    return 4;
  }
  
  // support for 5-bytes UTF8 would go here.
  decoding_error_policy<ErrorPolicy>::execute();
  cp = replacement;
  return -3;
}
  
# undef advance_and_return
  
/// @related `next(ByteIter, ByteIter, u32)`.
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter >
opt_always_inline
int next(ByteIter && first, ByteIter && last, rune & cp) {
  return next<replacement, ErrorPolicy>(first, last, cp);
}
  
/// Similar to `next(ByteIter, ByteIter, u32)` but returns the decoded
/// rune and treats number of bytes as the output parameter. Maybe be useful
/// for certain access modes to avoid subsequent rune copying. On return,
/// `first` will point to the first unconsumed byte in the iterator's range.
/// If you want to ignore a decoding erorr, rewind `first` by "the number of
/// bytes consumed + 1" and carry on decoding from the next byte. Null-runes
/// are handled as normal runes.
/// @param first Begin iterator.
/// @param last End iterator.
/// @param nbytes Output parameter for number of bytes consumed.
/// @tparam replacement The rune to use to report decoding error.
/// @tparam ErrorPolicy Error handling policy tag.
/// @tparam ByteIter A byte `InputIterator`.
/// @return The decoded rune on success, `error_rune` on failure. On success,
///         `nbytes` will be set to the number of bytes consumed, on failure,
///          to the negative of the number of bytes consumed.
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter >
opt_always_inline
rune next_rune(ByteIter & first, ByteIter & last, int & nbytes) {
  rune cp;
  nbytes = next<replacement, ErrorPolicy>(first, last, cp);
  return cp;
}
  
/// @related `next_rune(ByteIter, ByteIter, int)`.
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter >
opt_always_inline
rune next_rune(ByteIter && first, ByteIter && last, int & nbytes) {
  return next_rune<replacement, ErrorPolicy>(first, last, nbytes);
}
  
/// @related `next_rune(ByteIter, ByteIter, int)`.
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter >
opt_always_inline
rune next_rune(ByteIter && first, ByteIter && last) {
  int nbytes;
  return next_rune<replacement, ErrorPolicy>(first, last, nbytes);
}
  
/// Validates and decodes the next rune from a byte sequence. Whilst the
/// function is faster than other overloads, it can only be used if you are
/// sure that the iterator provided is null-terminated, for example, a null-
/// terminated `const char *`. The caller must advance the iterator by the
/// number of bytes returned for a subsequent call. If you want to ignore a
/// decoding error, advance the iterator by the number of bytes returned and
/// carry on. If the iterator is not null-terminated and its range is shorter
/// than the required bytes, a faulty memory access will occur leading to
/// undefined behaviour. Required bytes count may be safely assumed to be 4
/// (may be a waste for many codepoints though).
/// @param it an input byte iterator for the byte sequence.
/// @param cp an output argument to write the decoded rune to.
/// @tparam replacement The rune to use to report decoding error.
/// @tparam ErrorPolicy Error handling policy tag.
/// @tparam NullTerminatedByteIt a byte `InputIterator` that signals its range
/// end by a null-byte.
/// @return on success, the number of bytes used to encode the rune together
/// with setting `cp` to the decoded rune, on failure, always return -1 and
/// sets `cp` to `replacement`.
/// @requires iterator must be at least dereferenceable for one time.
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class NullTerminatedByteIt >
int next(NullTerminatedByteIt it, rune & cp) {

  using namespace details;
  cp = ensure_byte(it);
  i32 c1, c2, c3;

  // 1-byte sequence: 00 - 7f (t1)
  if (cp < state::tx) {
    return 1;
  }

  // 2-byte: 0080 - 07fff (t2 tx)
  c1 = ensure_byte(++it) ^ state::tx;

  if (c1 & state::testx)
    goto bad;

  if (cp < state::t3) {
    if (cp < state::t2)
      goto bad;
    cp = ((cp << state::bx) | c1) & state::r2;
    if (cp <= state::r1)
      goto bad;
    
    return 2;
  }

  // 3-byte: 0800 - ffff (t3 tx tx)
  c2 = ensure_byte(++it) ^ state::tx;

  if (c2 & state::testx)
    goto bad;

  if (cp < state::t4) {
    cp = ((((cp << state::bx) | c1) << state::bx) | c2) & state::r3;
    if (cp <= state::r2)
      goto bad;
    
    return 3;
  }

  // 4-byte: 010000 - 1fffff (t4 tx tx tx)
  c3 = ensure_byte(++it) ^ state::tx;

  if (c3 & state::testx)
    goto bad;

  if (cp < state::t5) {
    cp = ((((((cp << state::bx) | c1) << state::bx) | c2) << state::bx) | c3)
          & state::r4;
    if (cp <= state::r3 || cp > integer(rune_traits::max))
      goto bad;
    
    return 4;
  }
  
bad:
  decoding_error_policy<ErrorPolicy>::execute();
  cp = replacement;
  return -1;
}

/// A safer overload of `next(NullTerminatedByteIt, rune&)` which does not
/// require the iterator to have null-terminated range. If you know for sure
/// the iterator is long enough or is null-terminated, the unbounded version
/// is more efficient. However, if the input sequence contains null-runes,
/// this version might be more appropriate as it handles them as normal runes.
/// For return value and iterator semantics see the other overload.
/// @param it A byte `InputIterator`.
/// @param n  Length of the iterator's range.
/// @param cp A rune output parameter that will be set to the decoded rune
///            on success or `error_rune` on failure.
/// @tparam replacement The rune to use to report decoding error.
/// @tparam ErrorPolicy Error handling policy tag.
/// @tparam InputByteIt A byte `InputIterator`.
/// @return Number of bytes consumed from the iterator, or 1 on error.
/// @see `int next(NullTerminatedByteIt, rune&)`
/// @requires Iterator must be at least dereferenceable for one time.
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class InputByteIt >
int next(InputByteIt c, size_t n, rune & cp) {

  using namespace details;
  cp = details::ensure_byte(c);
  i32 c1, c2, c3;

  if (n < 1)
    goto bad_buffer;

  // 1-byte sequence: 00 - 7f (t1)
  if (cp < state::tx) {
    return 1;
  }

  if (n < 2)
    goto bad_buffer;

  // 2-byte: 0080 - 07fff (t2 tx)
  c1 = details::ensure_byte(++c) ^ state::tx;

  if (c1 & state::testx)
    goto bad;

  if (cp < state::t3) {
    if (cp < state::t2)
      goto bad;
    cp = ((cp << state::bx) | c1) & state::r2;
    if (cp <= state::r1)
      goto bad;
    
    return 2;
  }

  if (n < 3)
    goto bad;

  // 3-byte: 0800 - ffff (t3 tx tx)
  c2 = details::ensure_byte(++c) ^ state::tx;

  if (c2 & state::testx)
    goto bad;

  if (cp < state::t4) {
    cp = ((((cp << state::bx) | c1) << state::bx) | c2) & state::r3;
    if (cp <= state::r2)
      goto bad;
    
    return 3;
  }

  if (n < 4)
    goto bad_buffer;

  // 4-byte: 010000 - 1fffff (t4 tx tx tx)
  c3 = details::ensure_byte(++c) ^ state::tx;

  if (c3 & state::testx)
    goto bad;

  if (cp < state::t5) {
    cp = ((((((cp << state::bx) | c1) << state::bx) | c2)
          << state::bx) | c3) & state::r4;
    if (cp <= state::r3 || cp > integer(rune_traits::max))
      goto bad;
    
    return  4;
  }

bad_buffer:
  memory_error_policy<ErrorPolicy>::execute();
  cp = replacement;
  return -1;

bad:
  decoding_error_policy<ErrorPolicy>::execute();
  cp = replacement;
  return -1;
}
  
/// Validate and decode the previous rune in the iterator's range.
///
/// @param first Iterator pointing to the range's start.
/// @param it Iterator pointing to the current position in the range.
///
/// @tparam replacement In case of decoding error replace sequence with this.
/// @tparam ErrorPolicy Error handling policy, defualt is replace and continue.
/// @tparam Byte Iter A byte `BidirectionalIterator`.
///
/// @return The previous rune wrt where the iterator currently points to, on
/// decoding error, the bad sequence is either replaced with `replacement` or
/// `bad_encoding` is thrown according to the error policy.
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter >
inline rune previous(ByteIter & first, ByteIter & it) {
  while (is_trail(ensure_byte(--it)))
    if (first == it) {
      details::decoding_error_policy<ErrorPolicy>::execute();
      return replacement;
    }
  auto copy = it;
  return next<replacement, ErrorPolicy>(copy);
}
  
/// @related `previous<replacement, ErrorPolicy, ByteIter>(ByteIter&, ByteIter&)`
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter >
inline rune previous(ByteIter && first, ByteIter && it) {
  while (is_trail(ensure_byte(--it)))
    if (first == it) {
      details::decoding_error_policy<ErrorPolicy>::execute();
      return replacement;
    }
  return next<replacement, ErrorPolicy>(it);
}
  
/// Validate and decode the previous rune in the iterator's range.
/// This function may be a faster version but will have undefined behaviour if:
///   - the byte sequence up to the iterator's range start does not start
///     with a lead byte, i.e., UTF-8 is invalid.
///   - if the iterator is currently at the start of its range.
/// if unsure about these conditions, use the safer version
/// `previous(first, last)`.
///
/// @param it Iterator to traverse.
///
/// @tparam replacement In case of decoding error replace sequence with this.
/// @tparam ErrorPolicy Error handling policy, the default is replace and
/// continue.
///
/// @tparam Byte Iter A byte `BidirectionalIterator`.
///
/// @return The previous rune wrt where the iterator currently points to, on
/// decoding error, the bad sequence is either replaced with `replacement` or
/// `bad_encoding` is thrown according to the error policy.
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter >
inline rune previous(ByteIter & it) {
  while (is_trail(ensure_byte(--it))) /* noop */;
  auto copy = it;
  return next<replacement, ErrorPolicy>(copy);
}
  
/// @related `previous<replacement, ErrorPolicy, ByteIter>(ByteIter&)`
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter >
inline rune previous(ByteIter && it) {
  while (is_trail(ensure_byte(--it))) /* noop */;
  return next<replacement, ErrorPolicy>(it);
}
  
/// Advance the iterator forward by a number of runes equal to `count`.
/// On return `it` will be pointing to the first byte of the rune following
/// the `count` of runes traversed or to the end of the iterator's range if
/// the range is not long enough.
///
/// @param it Iterator to traverse.
/// @param end The next-to-last iterator indicating range's end.
///
/// @tparam ErrorPolicy Error handling policy, defualt is replace and continue.
/// @tparam ByteIter A byte input iterator of any type.
///
/// @throw `bad_encoding` if an invalid byte sequence is encountered if
/// allowed by the error policy.
/// @throw `memory_error` if within a single byte sequence the expected number
/// of bytes is not met if allowed bye the error policy.
template<
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter, class Distance >
inline void forward(ByteIter & it, ByteIter & end, Distance count) {
  for (Distance i = 0; i < count && it != end; ++i)
    next<error_rune, ErrorPolicy>(it, end);
}
  
/// @related forward<ErrorPolicy, ByteIter, Distance>(ByteIter&, ByteIter&, Distance)
/// @return Iterator pointing to the rune following `n` number of runes or
/// `end` if the range is not long enough.
template<
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter, class Distance >
inline ByteIter forward(ByteIter && it, ByteIter && end, Distance count) {
  for (Distance i = 0; i < count && it != end; ++i)
    next<error_rune, ErrorPolicy>(it, end);
  return it;
}

/// Advance the iterator forward by a number of runes equal to `n`.
/// On return `it` will be pointing to the first byte of the rune following
/// the `count` of runes traversed. Iterator's range must be long enough or
/// undefined behaviour may occur.
///
/// @param it Iterator to traverse.
/// @param n The number of runes to skip over.
///
/// @tparam ErrorPolicy Error handling policy, defualt is replace and continue.
/// @tparam ByteIter A byte input iterator of any type.
template<
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter, class Distance >
inline void forward(ByteIter & it, Distance n) {
  for (Distance i = 0; i < n; ++i)
    next<error_rune, ErrorPolicy>(it);
}
  
/// @related forward<ErrorPolicy, ByteIter, Distance>(ByteIter&, Distance)
/// @return Iterator pointing to the rune following `n` number of runes.
template<
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter, class Distance >
inline ByteIter forward(ByteIter && it, Distance n) {
  for (Distance i = 0; i < n; ++i)
    next<error_rune, ErrorPolicy>(it);
  return it;
}
  
/// Move the iterator back by `n` runes. `it` will be pointing to the rune
/// perceeding the current position by `n` runes or to `first` if the range
/// is not long enough and the error policy is not meant to throw.
template<
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter,
  class Distance >
inline void rewind(ByteIter & first, ByteIter & it, Distance n) {
  for (Distance i = 0; i < n; ++i)
    while (is_trail(ensure_byte(--it)))
      if (first == it)
        details::decoding_error_policy<ErrorPolicy>::execute();
}

/// @related rewind<ErrorPolicy, ByteIter, Distance>(ByteIter&, ByteIter&, Distance)
template<
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter,
  class Distance >
inline void rewind(ByteIter && first, ByteIter && it, Distance n) {
  rewind(first, it, n);
}

/// A faster version of `rewind(ByteIter&, ByteIter&, Distance)` that assume
/// the iterator's range is long enough and UTF-8 byte sequences are valid.
/// Produced undefined behaviour if these assumptions do not hold.
template< class ByteIter, class Distance >
inline void rewind(ByteIter & it, Distance n) {
  for (Distance i = 0; i < n; ++i)
    while (is_trail(ensure_byte(--it))) /* noop */;
}
  
/// @related rewind<ErrorPolicy, ByteIter, Distance>(ByteIter&, Distance)
/// @return Iterator pointing to the rune preceeding `n` runes before the
/// current rune.
template< class ByteIter, class Distance >
inline ByteIter rewind(ByteIter && it, Distance n) {
  rewind(it, n);
  return it;
}
  
/// Validate and decode a UTF-8 sequence into an output iterator `out`.
/// For each invalid subsequence, `replacement` will be written to `out`.
/// On return `first` will point to the byte next to the last byte decoded.
/// @param first Begin iterator.
/// @param end End iterator.
/// @param out Output iterator to write decoded runes to.
/// @tparam replacement Replacement rune for bad sequences, defaults to
///         `error_rune`.
/// @tparam ErrorPolicy Error handling policy, defaults to replace and
///         continue. Set it to throw_on_error_policy_tag to throw on errors.
/// @tparam ByteIter A byte iterator of any input-kind.
/// @tparam RuneOutputIt A rune `OutputIterator`.
/// @return The output iterator next-to-last decoded rune.
/// @throw `bad_encoding` if an invalid sequence is encountered and error
///        policy allows throwing.
/// @throw `memory_error` if a the iterator's range is not long enough to
///        accomodate valid sequences and the error policy allows throwing.
template< rune replacement = error_rune, class ByteIter, class RuneOutputIt,
          class ErrorPolicy = default_error_policy_tag >
inline RuneOutputIt decode(ByteIter& first, ByteIter& last, RuneOutputIt out) {
  
  using namespace details;

  int bytes {0};
  rune cp;
  while (true) {
    bytes = utf::next<replacement>(first, last, cp);
    *out = cp;
    ++out;
    if (bytes < 0) {
      decoding_error_policy<ErrorPolicy>::execute(first, last, bytes);
      if (first == last)
        break;
      advance_iterator(first, -bytes-1);
      continue;
    }
    
    if (first == last)
      break;
  }
  return out;
}
  
/// @related `decode(ByteIter& first, ByteIter& last, RuneOutputIt out)`.
template< rune replacement = error_rune, class ByteIter, class RuneOutputIt,
          class ErrorPolicy = default_error_policy_tag >
inline RuneOutputIt decode(
  ByteIter && first, ByteIter && last, RuneOutputIt out
) {
  return decode(first, last, std::move(out));
}

/// Validate and decode a UTF-8 byte sequence into a runes pointer.
/// For a previously allocated buffer, this version could possibly allow for
/// better performance. On return `first` will point to the byte next to the
/// last byte decoded.
/// @related `decode(ByteIter, ByteIter, RuneOutputIt)`.
template< rune replacement = error_rune,
          class ErrorPolicy = default_error_policy_tag,
          class ByteIter, class RuneType >
inline RuneType* decode(ByteIter & it, std::size_t length, RuneType * out) {
  
  using namespace details;
  
  int bytes {0};
  auto end = it + length;
  while (true) {
    bytes = next<replacement>(it, end, *out);
    ++out;
    if (bytes < 0) {
      decoding_error_policy<ErrorPolicy>::execute(it, end, bytes);
      if (it == end)
        break;
      it += -bytes - 1;
      continue;
    }
    
    if (it == end)
      break;
  }
  return out;
}
  
/// @related `decode(ByteIter&, size_t, RuneType*)`.
template< rune replacement = error_rune,
          class ErrorPolicy = default_error_policy_tag,
          class ByteIter, class RuneType >
inline RuneType* decode(ByteIter && it, std::size_t length, RuneType * out) {
  return decode(it, length, out);
}
  
/// Validate and decode a UTF-8 byte sequence into a runes pointer.
/// The iterator's range must be null-terminated. This version offers best
/// performance if you are sure the range is null-terminated.
/// @related `decode(ByteIter, ByteIter, RuneOutputIt)`.
template< rune replacement = error_rune, class ByteIter, class RuneType,
          class ErrorPolicy = default_error_policy_tag>
inline RuneType* decode(ByteIter it, RuneType * out) {
  
  using namespace details;
  
  int bytes {0};
  while (*it) {
    bytes = next<replacement>(it, *out);
    ++out;
    if (bytes > 0) {
      it += bytes;
    } else {
      decoding_error_policy<ErrorPolicy>::execute(*it);
      it += -bytes;
    }
  }
  return out;
}
  
// No-op rune validator to be used with `encode` if not validation is to be
// done.
struct null_validator {
  constexpr static rune validate(rune cp) noexcept { return cp; }
};
  
// A rune validator to be used with `encode` that replaces an invalid rune
// with a replacement rune which defaults to `error_rune`. Note that according
// to this validate, `error_rune` is valid as it is in the code points legal
// range and is not a surrogate.
template< rune replacement = error_rune >
struct replacing_validator {
  constexpr static rune validate(rune cp) noexcept {
    return is_valid(cp) ? cp : replacement;
  }
};
  
// A rune validator that throws the specified exception if the rune is not
// valid according to `is_valid(rune)`.
template< class Exception >
struct exception_validator {
  inline static rune validate(rune cp) {
    if (! is_valid(cp))
      throw Exception{};
    return cp;
  }
};

/// Encode and validate a sequence of Unicode codepoints into a UTF-8 byte
/// sequence stored into `out`. Validation is performed by the specified
/// validator which defaults to `replacing_validator<>`. A customer validator
/// should have a static member function `rune validate(rune)`. The default
/// behaviour is to replace invalid codepoints with `error_rune`. To disable
/// validation altogether use `null_validator` as the validator class.
/// Returns an output iterator pointing to the next byte after the last byte
/// encoded.
template<
  class Validator = replacing_validator<>,
  class RuneIter,
  class ByteOutputIter >
inline ByteOutputIter encode(
  RuneIter & first, RuneIter & last, ByteOutputIter out
) {
  
  int bytes = 0;
  while (true) {
    bytes = to_bytes(Validator::validate(*first), out);
    if (++first == last)
      break;
  }
  return out;
}
  
/// @related `ByteOutputIter encode(RuneIter&, RuneIter&, ByteOutputIter)`.
template<
  class Validator = replacing_validator<>,
  class RuneIter,
  class ByteOutputIter >
inline ByteOutputIter encode(
  RuneIter && first, RuneIter && last, ByteOutputIter out
) {
  return encode(first, last, std::move(out));
}
  
/// @related `ByteOutputIter encode(RuneIter&, RuneIter&, ByteOutputIter)`.
template< class RuneIter, class ByteOutputIter >
inline ByteOutputIter encode(
  RuneIter it, std::size_t length, ByteOutputIter out
) {
  return encode(std::move(it), it + length, std::move(out));
}
  
/// Encode and validate a Unicode codepoints sequence to UTF-8. The iterator
/// `it` must be have a null-terminated range. Returns the number of bytes
/// generated by the encoding.
/// @related `ByteOutputIter encode(RuneIter&, RuneIter&, ByteOutputIter)`.
template<
  class Validator = replacing_validator<>,
  class RuneIter,
  class ByteOutputIter >
inline std::size_t encode(RuneIter it, ByteOutputIter out) {
  int bytes = 0;
  size_t total = 0;
  while (*it) {
    bytes = to_bytes(Validator::validate(*it), out);
    total += bytes;
    ++it;
  }
  return total;
}
  
/// Return true if the current UTF-8 byte sequence that the iterator points
/// to with maximum range equal to (first, last] is valid. Sets `consumed`
/// to the number of bytes consumed from the iterator.
template< class ByteIter >
inline bool is_valid_rune(
  ByteIter first, ByteIter last, int & consumed
) {
  rune cp;
  consumed = next(std::move(first), std::move(last), &cp);
  return consumed > 0;
}

/// @related `bool is_valid_rune(ByteIter, ByteIter, int&)`.
template< class ByteIter >
inline bool is_valid_rune(ByteIter first, ByteIter last) {
  int consumed;
  next_rune(std::move(first), std::move(last), consumed);
  return consumed > 0;
}
  
/// Return true if the current UTF-8 byte sequence that the iterator points
/// to is valid. The iterator's range must be null-terminated or long enough
/// to contain a rune (a rune is at most 4 bytes long). Sets `consumed`
/// to the number of bytes consumed from the iterator.
template< class ByteIter >
inline bool is_valid_rune(ByteIter it) {
  rune cp;
  return next(it, cp) > 0;
}
 
/// @related `bool is_valid_rune(ByteIter)`.
template< class ByteIter >
inline bool is_valid_rune(ByteIter it, int & consumed) {
  rune cp;
  consumed = next(it, cp);
  return consumed > 0;
}
  
/// Test whether a codepoint sequence is complete. The sequence must be either
/// valid or partially valid, i.e., a prefix of a possibly valid sequence.
/// Given invalid sequence, the test result returned is meaningless.
template< class ByteIter >
inline bool is_complete(ByteIter first, ByteIter last) {
  
  using namespace details;
  
  if (first != last) {
    rune r = details::ensure_byte(first);

    if (r < state::tx)
      return true;

    if (++first != last) {
      if (r < state::t3)
        return true;
      if (++first != last) {
        return r < state::t4 || ++first != last;
      }
    }
  }

  return false;
}
  
/// Test whether a codepoint sequence is complete. The sequence must be either
/// valid or partially valid, i.e., a prefix of a possibly valid sequence.
/// Given invalid sequence, the test result returned is meaningless.
template< class ByteIter >
inline bool is_complete(ByteIter it, int n) {

  using namespace details;
  if (n > 0) {
    rune r = details::ensure_byte(it);
    if (r < state::tx)
      return true;

    if (n > 1) {
      if (r < state::t3)
        return true;
      if (n > 2) {
        return r < state::t4 || n > 3;
      }
    }
  }

  return false;
}
  
/// Return the number of valid runes in the UTF-8 sequence (first, last].
/// Note that validity is subject to `replacement`. In other words, this is
/// the number of runes you would get if you decode this byte sequence using
/// the same error replacement.
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter >
inline std::size_t runes_size(ByteIter first, ByteIter last) {
  
  using namespace details;

  rune c;
  size_t n = 0;

  for (; first != last; ++n) {

    c = ensure_byte(first);
    if (c < integer(rune_traits::same)) {
      ++first;
      continue;
    }
    if (!is_complete(first, last))
      break;
    // if invalid => replacement
    next<replacement, ErrorPolicy>(first, last, c);
  }

  return n;
}

/// Return the number of bytes required to encode a rune `cp` to UTF-8.
/// If the codepoint is range-invalid, return 0, i.e., this function returns
/// the reference byte-size for a codepoint.
constexpr int byte_size(rune cp) noexcept {
  # if defined(__GNUC__) || defined(__clang__) || defined(__llvm__)
  using namespace details;
//  switch (cp) {
//    case 0                        ... integer(state::r1): return 1;
//    case (integer(state::r1) + 1) ... integer(state::r2): return 2;
//    case (integer(state::r2) + 1) ... integer(state::r3): return 3;
//    case (integer(state::r3) + 1) ... (integer(rune_traits::max)-1): return 4;
//    default:
//      return 0;
//  }
  switch (cp) {
    case 0x0000  ... 0x007F:   return 1;
    case 0x0080  ... 0x07FF:   return 2;
    case 0x0800  ... 0xFFFF:   return 3;
    case 0x10000 ... 0x10FFFF: return 4;
    default:
      return 0;
  }
  # else
  return
    (cp > -1) ?
      (cp <= state::r1) ? 1 :
        (cp <= state::r2) ? 2 :
          (cp <= state::r3) ? 3 :
            (cp < integer(rune_traits::max)) ? 4 : 0
    0;
  # endif
}
  
/// Return the number of bytes required to encode a rune `cp` to UTF-8.
/// If the codepoint is range-invalid, return 0. This function is a helper
/// to specialise for certain runes that frequently processed to avoid
/// possible overhead. For example a specialisation for `error_rune` is
/// provided to use with constant complexity in other functions.
template< rune cp >
constexpr int byte_size() noexcept {
  return byte_size(cp);
}
  
/// Specialisation for `byte_size<rune>()` for `error_rune`.
template<>
constexpr int byte_size<error_rune>() noexcept {
  return 3;
}
  
/// Return the number of bytes required to encode a rune `cp`. If the codepoint
/// is range-invalid, return the byte-size of a replacement rune, i.e.,
/// this function returns the number of bytes required for a codepoint under
/// a specific UTF-8 decoding.
template< rune replacement = error_rune >
constexpr int decoded_byte_size(rune cp) noexcept {
  # if defined(__GNUC__) || defined(__clang__) || defined(__llvm__)
  using namespace details;
//  switch (cp) {
//    case 0                        ... integer(state::r1): return 1;
//    case (integer(state::r1) + 1) ... integer(state::r2): return 2;
//    case (integer(state::r2) + 1) ... integer(state::r3): return 3;
//    case (integer(state::r3) + 1) ... (integer(rune_traits::max)-1): return 4;
//    default:
//      return byte_size<replacement>();
//  }
  switch (cp) {
    case 0x0000  ... 0x007F:   return 1;
    case 0x0080  ... 0x07FF:   return 2;
    case 0x0800  ... 0xFFFF:   return 3;
    case 0x10000 ... 0x10FFFF: return 4;
    default:
      return byte_size<replacement>();
  }
  # else
  return
    (cp > -1) ?
      (cp <= state::r1) ? 1 :
        (cp <= state::r2) ? 2 :
          (cp <= state::r3) ? 3 :
            (cp < integer(rune_traits::max)) ? 4 : byte_size<replacement>()
    byte_size<replacement>();
  # endif
}
  
/// Return the decoding specific UTF-8 byte-size of a codepoint sequence.
template< rune replacement = error_rune, class RuneIter >
inline std::size_t decoded_byte_size(RuneIter first, RuneIter last) {
  
  using namespace details;
  size_t nb = 0;
  for (; first != last; ++first)
    nb += decoded_byte_size<replacement>(*first);
  return nb;
}
  
/// @related `std::size_t decoded_byte_size(RuneIter, RuneIter)`.
template< rune replacement = error_rune, class RuneIter >
inline std::size_t decoded_byte_size(RuneIter it, std::size_t n) {
  return decoded_byte_size(it, it+n);
}
  
/// Return true if the codepoint currently pointed to by `first` is valid.
template< class ByteIter >
inline bool is_valid_codepoint(ByteIter first, ByteIter last) {
  rune cp;
  return (next(first, last, cp) > 0 && is_valid(cp));
}
  
/// Return true if the codepoint currently pointed to by `first` is valid.
/// Undefined behaviour if the iterator's range is not long enough to check
/// the first rune, 1-4 bytes and the range is not null-terminated.
template< class ByteIter >
inline bool is_valid_codepoint(ByteIter first) {
  rune cp;
  return (next(first, cp) > 0 && is_valid(cp));
}
  
/// Return true if the codepoint currently pointed to by `first` is valid.
template< class ByteIter >
inline bool is_valid_codepoint(ByteIter first, std::size_t n) {
  rune cp;
  return (next(first, n, cp) > 0 && is_valid(cp));
}
  
// TODO: test
/// Seeks the iterator `first` to the beginning of the next invalid UTF-8 byte
/// sequence. If the byte sequence is all valid, `first == last` on return.
/// "Invalid" means that the byte sequence cannot be decoded, whether you
/// are welling to replace an invalid sequence is a different matter.
template< class ByteIter >
inline void seek_invalid(ByteIter & first, ByteIter & last) {
  int nbytes;
  rune cp;
  while (first != last && ((nbytes = next(first, last, cp)) > 0))
    ;
  // position the iterator at the start of the error sequence not the end
  if (nbytes < 0)
    advance_iterator(first, nbytes);
}
  
// TODO: test
/// @related `void seek_invalid(ByteIter&, ByteIter &).
template< class ByteIter >
inline ByteIter seek_invalid(ByteIter && first, ByteIter && last) {
  seek_invalid(first, last);
  return first;
}
  
// TODO: test
/// Return true if the UTF-8 byte sequence (first, last] contains an invalid
/// subsequence.
template< class ByteIter >
inline bool contains_invalid(ByteIter & first, ByteIter last) {
  seek_invalid(first, last);
  return first != last;
}
  
// TODO: test
/// Return true if the UTF-8 byte sequence (first, last] contains an invalid
/// subsequence. `first` is passed as a reference so you can seek to the first
/// unsafe codepoint if you need to in a single call.
template< class ByteIter >
inline bool contains_invalid(ByteIter && first, ByteIter && last) {
  return contains_invalid(first, last);
}
  
  
// TODO: test
/// Position the iterator `first` at the first unsafe codepoint or at `last`
/// if no unsafe codepoints were found. An invalid codepoint is considered
/// unsafe.
template< class ByteIter >
inline void seek_unsafe(ByteIter & first, ByteIter & last) {
  int nbytes;
  rune cp;
  while (
    first != last &&
    ((nbytes = next(first, last, cp)) > 0) &&
    is_safe(cp)
  ) ;
  // position the iterator at the start of the unsafe sequence not the end
  if (nbytes < 0)
    advance_iterator(first, nbytes);
}
  
template< class ByteIter >
inline void seek_unsafe(ByteIter & first, ByteIter & last, bool & is_valid) {
  int nbytes;
  rune cp;
  while (
    first != last &&
    ((nbytes = next(first, last, cp)) > 0) &&
    is_safe(cp)
  ) ;
  // position the iterator at the start of the unsafe sequence not the end
  if (nbytes < 0) {
    advance_iterator(first, nbytes);
    is_valid = false;
  } else {
    is_valid = true;
  }
}
  
// TODO: test
/// @related `void seek_unsafe(ByteIter&, ByteIter &).
template< class ByteIter >
inline ByteIter seek_unsafe(ByteIter && first, ByteIter && last) {
  seek_unsafe(first, last);
  return first;
}
  
// TODO: test
/// Return true if the UTF-8 byte sequence (first, last] contains an unsafe
/// codepoint. `first` is passed as a reference so you can seek to the first
/// unsafe codepoint if you need to in a single call. An invalid codepoint
/// is considered unsafe.
template< class ByteIter >
inline bool contains_unsafe(ByteIter & first, ByteIter last) {
  seek_unsafe(first, last);
  return first != last;
}
  
/// Return true if the UTF-8 byte sequence (first, last] contains an unsafe
/// codepoint.
template< class ByteIter >
inline bool contains_unsafe(ByteIter && first, ByteIter && last) {
  return contains_unsafe(first, last);
}
  
/// The distance between two codepoints, i.e., the number of codepoints to
/// traverse starting from the first to arrive at the second. For example,
/// [start, end] has a distance of 2, [start, a, end] has a distance of 3,
/// [start] has a distance of 0.
/// Precondition `first <= last`.
template< class ByteIter >
inline details::difference_type<ByteIter>
distance(ByteIter first, ByteIter last) {
  int d = 0;
  for (; first != last; ++first)
    d += !is_trail(ensure_byte(first));
  return d;
}
  
// Functions in this namespace are faster but are only applicable to UTF-8
// sequences that are surely valid. For example, if you previously encoded
// Unicode text using this library and decoding these sequences back or
// the original text comes from a trusted source. Surely, this source is
// not the Web.
namespace unchecked {
  
template< class ByteType >
opt_always_inline
constexpr int byte_size_from_lead(ByteType lead) noexcept {
  return "\1\1\1\1\1\1\1\1\1\1\1\1\2\2\3\4"[mask8(lead) >> 4];
}
  
template< class ByteIter >
rune next(ByteIter & it) {
  
  using namespace details;
  
  rune cp = ensure_byte(it);
  auto length = byte_size_from_lead(ensure_byte(it));
  
  switch (length) {
    case 1:
      break;
    case 2:
      ++it;
      cp = ((cp << 6) & state::r2) | (ensure_byte(it) & state::maskx);
      break;
    case 3:
      ++it;
      cp = ((cp << 12) & state::mask16) |
           ((ensure_bytes(it) << 6) & state::mask12);
      ++it;
      cp += ensure_byte(it) & state::maskx;
      break;
    case 4:
      ++it;
      cp = ((cp << 18) & state::mask21) |
           ((ensure_byte(it) << 12) & state::mask18);
      ++it;
      cp += (ensure_byte(it) << 6) & state::mask12;
      ++it;
      cp += ensure_byte(it) & state::maskx;
      break;
  }
  ++it;
  return cp;
}
  
template< class ByteIter >
inline rune peek(ByteIter it) {
  return next(it);
}
  
template< class ByteIter >
inline rune previous(ByteIter & it) {
  while (is_trail(ensure_byte(--it))) /* noop */;
  auto copy = it;
  return next(copy);
}

template< class ByteIter, class Distance >
inline void forward(ByteIter & it, Distance count) {
  int len = byte_size_from_lead(ensure_byte(it));
  for (Distance i = 0; i < count && len; ++i)
    advance_iterator(it, len);
}
  
template< class ByteIter >
inline std::size_t runes_size(ByteIter first, ByteIter last) {
  details::difference_type<ByteIter> len{0};
  int adv = byte_size_from_lead(ensure_byte(first));
  for (; std::distance(first, last) > 0 && adv; ++len)
    advance_iterator(first, adv);
  return len;
}
  
template< class ByteIter, class RuneOutputIt >
inline RuneOutputIt decode(ByteIter first, ByteIter last, RuneOutputIt out) {
  while (first != last) {
    *out = next(first);
    ++out;
  }
  return out;
}
  
template< class ByteIter, class RuneOutputIt >
inline RuneOutputIt decode(ByteIter first, std::size_t n, RuneOutputIt out) {
  auto last = first + n;
  while (first != last) {
    *out = next(first);
    ++out;
  }
  return out;
}
  
template< class RuneIter, class ByteOutputIter >
inline ByteOutputIter encode(
  RuneIter first, RuneIter last, ByteOutputIter out
) {
  int bytes = 0;
  while (true) {
    bytes = to_bytes(*first, out);
    if (++first == last)
      break;
  }
  return out;
}
  
} /* namespace unchecked */
  


template<
  class ByteIt,
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class Tag = std::input_iterator_tag >
class input_iterator {
public:
  
  using value_type        = rune;
  using reference         = value_type;
  using pointer           = value_type *;
  using iterator_category = Tag;
  using difference_type   =
    typename std::iterator_traits<ByteIt>::difference_type;
  
  input_iterator(const ByteIt it, const ByteIt end)
    : m_iter{ it }, m_end{ end } {}
  
  ByteIt base() const noexcept { return m_iter; }
  
  rune operator*() const {
    ByteIt first = m_iter; // next() expects two & or two &&
    return next<replacement, ErrorPolicy>(first, m_end);
  }
  
  friend bool operator==(
    const input_iterator & lhs, const input_iterator & rhs
  ) noexcept {
    return lhs.m_iter == rhs.m_iter;
  }
  
  friend bool operator!=(
    const input_iterator & lhs, const input_iterator & rhs
  ) noexcept {
    return !(lhs == rhs);
  }
  
  input_iterator & operator++() {
    next<replacement, ErrorPolicy>(m_iter, m_end);
    return *this;
  }
  
  input_iterator operator++(int) {
    input_iterator x = *this;
    next<replacement, ErrorPolicy>(m_iter, m_end);
    return x;
  }
  
protected:
  ByteIt m_iter;
  ByteIt m_end;
}; /* class input_iterator */
  
template<
  class ByteIt,
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class Tag = std::forward_iterator_tag >
class forward_iterator
  : public input_iterator<ByteIt, replacement, ErrorPolicy, Tag>
{
public:
  using input_iterator<ByteIt, replacement, ErrorPolicy, Tag>::input_iterator;
}; /* class forward_iterator */
  
template<
  class ByteIt,
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class Tag = std::bidirectional_iterator_tag >
class bidirectional_iterator
  : public forward_iterator<ByteIt, replacement, ErrorPolicy, Tag>
{
public:
  bidirectional_iterator(const ByteIt it, const ByteIt start, const ByteIt end)
    : forward_iterator<ByteIt, replacement, ErrorPolicy, Tag>{ it, end },
      m_start{ start }
  {
    assert((it > m_start && it < end) || (it == start) || (it == end) &&
           "provided iterator it out of range");
  }
  
  bidirectional_iterator & operator--() {
    previous<replacement, ErrorPolicy>(m_iter, m_end);
    return *this;
  }
  
  bidirectional_iterator operator--(int) {
    bidirectional_iterator x = *this;
    previous<replacement, ErrorPolicy>(m_iter, m_end);
    return x;
  }
  
private:
  using forward_iterator<ByteIt, replacement, ErrorPolicy, Tag>::m_iter;
  using forward_iterator<ByteIt, replacement, ErrorPolicy, Tag>::m_end;
  ByteIt m_start;
}; /* class bidirectional_iterator */
  
template<
  class ByteIt,
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class Tag = std::random_access_iterator_tag >
class random_access_iterator
  : public bidirectional_iterator<ByteIt, replacement, ErrorPolicy, Tag>
{
public:
  
  using bidirectional_iterator<
    ByteIt, replacement, ErrorPolicy, Tag>::bidrectional_iterator;
  
  using difference_type =
    typename std::iterator_traits<ByteIt>::difference_type;
  
  random_access_iterator operator+(difference_type x) const noexcept {
    return random_access_iterator{ m_iter + x, m_start, m_end };
  }
  
  random_access_iterator operator-(difference_type x) const noexcept {
    return random_access_iterator{ m_iter - x, m_start, m_end };
  }
  
  random_access_iterator & operator+=(difference_type x) noexcept {
    m_iter += x;
    return *this;
  }
  
  random_access_iterator & operator-=(difference_type x) noexcept {
    m_iter -= x;
    return *this;
  }
  
private:
  using bidirectional_iterator<ByteIt, replacement, ErrorPolicy, Tag>::m_iter;
  using bidirectional_iterator<ByteIt, replacement, ErrorPolicy, Tag>::m_start;
  using bidirectional_iterator<ByteIt, replacement, ErrorPolicy, Tag>::m_end;
}; /* class random_access_iterator */
  
template<
  class ByteIt,
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag >
using iterator =
  typename std::conditional<
    std::is_convertible<
      typename std::iterator_traits<ByteIt>::iterator_category,
      std::random_access_iterator_tag >::value,
    random_access_iterator<ByteIt, replacement, ErrorPolicy>,
    
    typename std::conditional<
      std::is_convertible<
        typename std::iterator_traits<ByteIt>::iterator_category,
        std::bidirectional_iterator_tag >::value,
      bidirectional_iterator<ByteIt, replacement, ErrorPolicy>,
    
      typename std::conditional<
        std::is_convertible<
          typename std::iterator_traits<ByteIt>::iterator_category,
          std::forward_iterator_tag >::value,
        forward_iterator<ByteIt, replacement, ErrorPolicy>,
        input_iterator<ByteIt, replacement, ErrorPolicy>
  
      >::type
    >::type
  >::type;
  
  
/// Find the first occurrence of rune `c` in the UTF-8 byte sequence
/// (first, last]. If `c` is not found, `return(it) == last` on return.
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class ByteIter >
ByteIter find(ByteIter first, ByteIter last, rune c) {

  rune c1;
  int n;
  rune r;

  // not part of UTF sequence
  if (c < integer(details::rune_traits::sync)) {

    while (details::ensure_byte(first) != static_cast<u8>(c)) {
      if (first == last)
        return std::move(last);
      ++first;
    }

    return std::move(first);
  }

  // advance_iterator() below may go beyond last in some cases
  while (std::distance(first, last) > 0) {
    
    c1 = details::ensure_byte(first);
    if (c1 < integer(details::rune_traits::same)) { // one byte rune
      if (c1 == c)
        return std::move(first);
      ++first;
      continue;
    }
    
    n = next<replacement, ErrorPolicy>(first, last, r);
    if (likely(n > 0)) {
      if (r == c) {
        advance_iterator(first, -n);
        return std::move(first);
      }
      // TODO: already advanced? test
//      advance_iterator(first, n);
    }
  }

  return std::move(last);
}
  
namespace unchecked {
template< class ByteIter >
ByteIter find(ByteIter first, ByteIter last, rune c) {

  rune c1;
  rune r;

  // not part of UTF sequence
  if (c < integer(details::rune_traits::sync)) {

    while (details::ensure_byte(first) != static_cast<u8>(c)) {
      if (first == last)
        return std::move(last);
      ++first;
    }

    return std::move(first);
  }

  // advance_iterator() below may go beyond last in some cases
  while (std::distance(first, last) > 0) {
    
    c1 = details::ensure_byte(first);
    if (c1 < integer(details::rune_traits::same)) { // one byte rune
      if (c1 == c)
        return std::move(first);
      ++first;
      continue;
    }
    
    r = next(first);
    if (r == c) {
      advance_iterator(first, -byte_size(r));
      return std::move(first);
    }
  }

  return std::move(last);
}
} /* namespace unchecked */
  
/// Find the first occurrence of rune `c` in the null-terminated UTF-8 byte
/// sequence (it, ...]. If `c` is not found, `*return(it) == 0` on return.
/// This function uses the most efficient byte sequence traversal functions
/// but the range must be null-terminated and cannot contain null characters
/// as they will be regarded as the range's end.
template<
  rune replacement = error_rune,
  class ErrorPolicy = default_error_policy_tag,
  class NullTerminatedByteIt >
NullTerminatedByteIt find(NullTerminatedByteIt it, rune c) {
  
  int nbytes;
  rune cp;
  
  if (c < integer(details::rune_traits::sync)) // not part of UTF sequence
    while (true) {
      if (*it == 0 || details::ensure_byte(it) == c)
        return it;
      ++it;
    }
  
  while(true) {
    cp = details::ensure_byte(it);
    if (cp < integer(details::rune_traits::same)) { // one byte rune
      if (cp == 0 || cp == c)
        return it;
      ++it;
      continue;
    }
    nbytes = next<replacement, ErrorPolicy>(it, cp);
    if (cp == c)
      return it;
    it += (nbytes > 0 ? nbytes : -nbytes);
  }
}
  
/// Find the first occurrence of rune `c` in the null-terminated UTF-8 byte
/// sequence (it, it+n]. If `c` is not found, `return(it) == it + n` on return.
/// The range (it, it+n] must be valid. Can be used to search for null
/// characters.
template< class ByteIter >
ByteIter find(ByteIter it, std::size_t length, rune c) {
  
  int nbytes;
  rune cp;
  ByteIter end = it + length;
  
  if (c < integer(details::rune_traits::sync)) { // not part of UTF sequence
    for (; it != end; ++it)
      if (*it == c) return it;
    return it;
  }
  
  while(it != end) {
    
    cp = details::ensure_byte(it);
    if (cp < integer(details::rune_traits::same)) { // one byte rune
      if (cp == 0 || cp == c) return it;
      ++it;
      continue;
    }
    
    nbytes = next(it, cp);
    if (cp == c) return it;
    it += (nbytes > 0 ? nbytes : -nbytes);
  }
  
  return end;
}
  
/// Finds a null-terminated codepoint subsequence in a larger or equal sequence.
/// Both sequences must be null-terminated.
template< class NullTerminatedRuneIt >
NullTerminatedRuneIt find(NullTerminatedRuneIt s1, NullTerminatedRuneIt s2) {

  rune c0 = *s2;
  rune c;
  if (c0 == 0)
    return s1;

  ++s2;

  NullTerminatedRuneIt it1, it2;

  for (NullTerminatedRuneIt it = find(s1, c0); *it ; it = find(++it, c0)) {
    it1 = it;
    for (it2 = s2; ; ++it2) {
      c = *it2;
      if (c == 0) return it;
      if (c != *++it1) break;
    }
  }

  return it1;
}
  
/// Find the last occurrence of a rune `c` in a null-terminated UTF-8 byte
/// sequence. If note found, `return(*it) == null`.
template< class NullTerminatedByteIt >
NullTerminatedByteIt rfind(NullTerminatedByteIt it, rune c) {
  
  int nbytes{0};
  rune cp;
  NullTerminatedByteIt found;
  
  if (c < integer(details::rune_traits::sync)) { // not part of a UTF sequence
    while(*it) {
      if (*it == c) found = it;
      ++nbytes;
      ++it;
    }
    return nbytes ? found : it;
  }
  
  found = it;
  while (true) {
    cp = details::ensure_byte(it);
    if (cp < integer(details::rune_traits::same)) {
      if (cp == 0) return found;
      if (cp == c) found = it;
      ++it;
      continue;
    }
    nbytes = next(it, cp);
    if (cp == c)
      found = it;
    it += nbytes;
  }
}

/// Find the last occurence of a rune in the UTF-8 byte sequence range
/// (it, it+n]. If not found `return(it) == it + n`.
template< class ByteIter >
ByteIter rfind(ByteIter it, std::size_t length, rune c) {
  
  int nbytes;
  rune cp;
  ByteIter end = it + length - 1;
  ByteIter found = end + 1;
  
  if (c < integer(details::rune_traits::sync)) { // not part of UTF sequence
    for (; length > 1; --length, --end)
      if (*end == c) return end;
    return nullptr;
  }
  
  ++end;
  while (it < end) {
    cp = details::ensure_byte(it);
    if (cp < integer(details::rune_traits::same)) { // one byte rune
      if (cp == c) found = it;
      ++it;
      continue;
    }
    
    nbytes = next(it, cp);
    if (cp == c)
      found = it;
    it += nbytes;
  }
  
  return found;
}
  
namespace unchecked {
template< class ByteIter >
ByteIter rfind(ByteIter it, std::size_t length, rune c) {
  
  rune cp;
  ByteIter end = it + length - 1;
  ByteIter found = end + 1;
  
  if (c < integer(details::rune_traits::sync)) { // not part of UTF sequence
    for (; length > 1; --length, --end)
      if (*end == c) return end;
    return nullptr;
  }
  
  ++end;
  while (it < end) {
    cp = details::ensure_byte(it);
    if (cp < integer(details::rune_traits::same)) { // one byte rune
      if (cp == c) found = it;
      ++it;
      continue;
    }
    
    cp = unchecked::next(it);
    if (cp == c)
      found = it;
    it += byte_size(cp);
  }
  
  return found;
}
} /* namesapce unchecked */

inline const rune * find(const rune * s, rune c) {

  if (c == 0) {
    while (*s) ++s;
    return s - 1;
  }

  for (rune c1 = *s; c1 != 0; ++s, c1 = *s)
    if (c1 == c)
      return s - 1;

  return nullptr;
}
  
inline const rune * find(const rune *s, std::size_t length, rune c) {
  
  for (std::size_t k = 0; k < length; ++k)
    if (s[k] == c) return s+k;
  return nullptr;
}
  
inline const rune *
find(const rune * s1, std::size_t s1len, const rune * s2, std::size_t s2len) {
  return std::search(s1, s1+s1len, s2, s2+s2len);
}
  
inline const char * find(const char * s1, const char * s2) {

  rune cp;
  int nbytes1 = next(s2, cp);
  if (cp <= integer(details::rune_traits::sync))
    return std::strstr(s1, s2);

  int nbytes2 = strlen(s2);

  for (const char * p = s1; (p = find(p, cp)) != 0; p += nbytes1)
    if (std::strncmp(p, s2, nbytes2) == 0)
      return p;

  return nullptr;
}
  
inline const char *
find(const char * s1, std::size_t s1len, const char * s2, std::size_t s2len) {
  return std::search(s1, s1+s1len, s2, s2+s2len);
}

inline const rune * rfind(const rune * s, rune c) {

  if (c == 0)
    return find(s, 0);

  while ((s = find(s, c)) != 0)
    ++s;

  return s;
}
  
inline const rune * rfind(const rune * s, std::size_t length, rune c) {
  for (; length > 1; --length)
    if (s[length-1] == c) return s+length-1;
  return nullptr;
}
  
inline const rune *
rfind(const rune * s1, std::size_t s1len, const rune * s2, std::size_t s2len) {
  auto s1_rbegin = std::make_reverse_iterator(s1);
  auto s1_rend   = std::make_reverse_iterator(s1 + s1len);
  auto s2_rbegin = std::make_reverse_iterator(s2);
  auto s2_rend   = std::make_reverse_iterator(s2 + s2len);
  return std::search(s1_rbegin, s1_rend, s2_rbegin, s2_rend).base();
}
  
inline const char *
rfind(const char * s1, std::size_t s1len, const char * s2, std::size_t s2len) {
  auto s1_rbegin = std::make_reverse_iterator(s1);
  auto s1_rend   = std::make_reverse_iterator(s1 + s1len);
  auto s2_rbegin = std::make_reverse_iterator(s2);
  auto s2_rend   = std::make_reverse_iterator(s2 + s2len);
  return std::search(s1_rbegin, s1_rend, s2_rbegin, s2_rend).base();
}

template< class ByteIter >
ByteIter rfind(ByteIter first, ByteIter last, rune c) {

  rune r;
  ByteIter it{ last };
  int nbytes;

  // not part of UTF sequence
  if (c < integer(details::rune_traits::sync)) {

    do {
      if (details::ensure_byte(first) == static_cast<u8>(c))
        it = first;
    } while (++first != last);
    return std::move(it);
  }

  while (true) {
    
    r = details::ensure_byte(first);
    if (r < integer(details::rune_traits::same)) { // one byte rune
      if (std::distance(first, last) <= 0) return std::move(it);
      if (r == c) it = first;
      ++first;
      continue;
    }
    
    nbytes = next(first, last, r);
    if (r == c)
      it = first;
    if (likely(nbytes > 0))
      advance_iterator(first, nbytes);
    else
      ++first;
  }

  return std::move(it);
}
  
namespace unchecked {
template< class ByteIter >
ByteIter rfind(ByteIter first, ByteIter last, rune c) {

  rune r;
  ByteIter it{ last };

  // not part of UTF sequence
  if (c < integer(details::rune_traits::sync)) {

    do {
      if (details::ensure_byte(first) == static_cast<u8>(c))
        it = first;
    } while (++first != last);
    return std::move(it);
  }

  while (true) {
    
    r = details::ensure_byte(first);
    if (r < integer(details::rune_traits::same)) { // one byte rune
      if (std::distance(first, last) <= 0) return std::move(it);
      if (r == c) it = first;
      ++first;
      continue;
    }
    
    r = next(first);
    if (r == c)
      it = first;
  }

  return std::move(it);
}
} /* namespace unchecked */

// For pointer-like iterators, e.g., const char *.
template< class NullTerminatedOctetIt >
inline std::size_t runes_size(NullTerminatedOctetIt s) {

  using namespace details;
  i32 c;
  size_t n = 0;
  rune r;
  for (;;) {
    c = ensure_byte(s);
    if (c < integer(rune_traits::same)) {
      if (c == 0)
        return n;
      ++s;
    } else {
      c = next(s, r);
      s += (c > 0 ? c : -c);
    }
    ++n;
  }
  return 0;
}

// For pointer-like iterators, e.g., const char *.
template< class NullTerminatedOctetIt >
inline std::size_t runes_size(NullTerminatedOctetIt s, std::size_t n) {

  i32 c;
  size_t m = 0;
  rune r;
  NullTerminatedOctetIt es = s + n;

  for (; s != es; ++m) {

    c = details::ensure_byte(s);
    if (c < integer(details::rune_traits::same)) {
      if (c == '\0')
        break;
      ++s;
      continue;
    }

//    if (!is_complete(s, es))
//      break;
    c = next(s, r);
    s += (c > 0 ? c : -c);
  }

  return m;
}

template< class RuneInputIt >
inline int compare(
  RuneInputIt s1first, RuneInputIt s1last,
  RuneInputIt s2first, RuneInputIt s2last
) {

  rune c1, c2;
  while(s1first != s1last && s2first != s2last) {
    c1 = *s1first;
    c2 = *s2first;
    ++s1first;
    ++s2first;
    if (c1 != c2) {
      if (c1 > c2)
        return 1;
      return -1;
    }
  }
  return 0;
}


inline int compare(const rune * s1, const rune * s2) {

  rune c1, c2;
  while(true) {
    c1 = *s1;
    c2 = *s2;
    ++s1;
    ++s2;
    if (c1 != c2) {
      if (c1 > c2)
        return 1;
      return -1;
    }
    if (c1 == 0)
      return 0;
  }
}

inline int compare(const rune * s1, const rune * s2, std::size_t n) {

  rune c1, c2;
  while (n > 0) {
    c1 = *s1;
    c2 = *s2;
    ++s1;
    ++s2;
    --n;
    if (c1 != c2) {
      if (c1 > c2)
        return 1;
      return -1;
    }
    if (c1 == 0)
      break;
  }
  return 0;
}
  
/* ****************************************************************** */

inline rune * copy(const rune * src, rune * dest) {
  while ((*dest = *src) != 0) {
    ++dest;
    ++src;
  }
  return dest;
}

inline rune * copy(const rune * src, rune * dest, std::size_t n) {

  for (std::size_t i = 0; i < n; ++i) {

    if ((*dest = *src) == 0) {
      ++dest;
      ++src;
      while (i < n) {
        *dest = 0;
        ++dest;
      }
      return dest;
    }

    ++dest;
    ++src;
  }

  return dest;
}
  
/* ****************************************************************** */

inline rune * ecopy(const rune * src, rune * e, rune * dest) {

  if (dest >= e)
    return dest;

  while ((*dest = *src) != 0) {
    ++dest;
    ++src;
    if (dest == e) {
      *--dest = '\0';
      break;
    }
  }

  return dest;
}
  
template< class RuneInputIt, class RuneOutputIt >
inline RuneOutputIt ecopy(
  RuneInputIt first, RuneInputIt last, RuneInputIt e, RuneOutputIt dest
) {
  if (std::distance(e, last) >= 0)
    return std::move(dest);

  for (; first != last; ++first, ++dest) {
    *dest = *first;
    if (*first == *e)
      break;
  }

  return std::move(dest);
}

inline char * ecopy(const char * src, char * e, char * dest) {

  char * end;

  if (dest >= e)
    return dest;

  end = (char *) memccpy(dest, src, '\0', e - dest);

  if (!end) {
    end = e - 1;
    while (end > dest && (*--end & 0xc0) == 0x80)
      ;
    *end = '\0';
  } else {
    --end;
  }
  return end;
}
  
/* ****************************************************************** */

inline rune * concat(rune * s1, const rune * s2) {
  copy(s2, const_cast<rune *>(find(s1, 0)));
  return s1;
}

inline rune * concat(rune * s1, const rune * s2, std::size_t n) {

  s1 = const_cast<rune*>(find(s1, 0));
  while ((*s1 = *s2) != 0) {
    ++s1;
    ++s2;
    if (--n == 1) {
      s1[-1] = 0;
      break;
    }
  }
  return s1;
}

} // namespace utf
END_DASKY_NAMESPACE


#endif /* DASKY_UTF_CODEC_H_ */
