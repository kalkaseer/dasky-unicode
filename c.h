//
//  utf_capi.h
//  dasky
//
//  Created by Kareem Alkaseer on 02/12/2017.
//
//

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#ifndef dasky_utf_capi_h
#define dasky_utf_capi_h

#if defined(__cplusplus)
extern "C" {
#endif

typedef char32_t u8_rune_t;
  

/// Return true if the byte buffer specified could possibly be decoded by
/// `to_rune`, false otherwise. Regardless of the return value, this function
/// only checks the possibility of decoding, not whether the buffer contains
/// a valid UTF-8 sequence. This function could be used by client code
/// that receive input one byte at a time and need to to recognise when a full
/// rune has arrived.
bool u8_rune_is_complete(const char * c, size_t n);
  
/// Return the number of complete runes contained in the first `n` bytes of
/// the specified byte buffer.
/// *Requires* that `s` is null-terminated.
size_t u8_runes_nsize(const char * s, size_t n);

/// Return the number of complete runes contained in the specified byte buffer.
/// *Requires* that `s` is null-terminated.
size_t u8_runes_size(const char * s);

/// Return the length of a null-terminated rune buffer.
size_t u8_runes_length(const u8_rune_t * s);
  
/// Return the number of bytes required to encode a sequence of runes into
/// a UTF-8 sequence.
size_t u8_runes_byte_size(const u8_rune_t * r, size_t n);

/// Return the number of bytes required to encode a rune `r` into a UTF-8
/// sequence.
size_t u8_rune_byte_size(u8_rune_t r);
  
/// Encodes one rune as at most rune_max_bytes into the specified buffer.
/// @return the number of bytes used for encoding the rune.
/// @requires buf is big enough to contain at most rune_max_bytes (4 bytes).
int u8_rune_to_bytes(u8_rune_t r, char * buf);
  
/// Decodes at most rune_max_bytes bytes from the specified buffer into
/// one rune. The function reads at most `n` bytes from the buffer.
/// @return number of bytes consumed from the buffer; on success `r` is
/// is set to the decoded rune, on failure `r` is set to `rune_error`.
int u8_bytes_to_rune(const char * buf, size_t n, u8_rune_t * r);

/// Decodes at most rune_max_bytes bytes from the specified buffer - which
/// must be null-terminated - into one rune.
/// Use this version if you are sure that the buffer is null-terminated
/// @return number of bytes consumed from the buffer; on success `r` is
/// is set to the decoded rune, on failure `r` is set to `rune_error`.
int u8_byte_to_rune(const char * buf, u8_rune_t * r);
  
const u8_rune_t * u8_find_rune(const u8_rune_t * s, u8_rune_t c);

const char * u8_find_rune_bytes(const char * s, u8_rune_t c);

const u8_rune_t * u8_rfind_rune(const u8_rune_t * s, u8_rune_t c);

const char * u8_rfind_rune_bytes(const char * s, u8_rune_t c);
  
const u8_rune_t * u8_substring(const u8_rune_t * s1, const u8_rune_t * s2);
  
const char * u8_substring_bytes(const char * s1, const char * s2);
  
int u8_compare(const u8_rune_t * s1, const u8_rune_t * s2);

int u8_ncompare(const u8_rune_t * s1, const u8_rune_t * s2, size_t n);
  
u8_rune_t * u8_copy(const u8_rune_t * src, u8_rune_t * dest);

u8_rune_t * u8_ncopy(const u8_rune_t * src, u8_rune_t * dest, size_t n);

u8_rune_t * u8_runes_ecopy(
  const u8_rune_t * src, u8_rune_t * e, u8_rune_t * dest);

char * u8_bytes_ecopy(const char * src, char * e, char * dest);
  
u8_rune_t * concat(u8_rune_t * s1, const u8_rune_t * s2);

u8_rune_t * nconcat(const u8_rune_t * s1, const u8_rune_t * s2, size_t n);

u8_rune_t u8_toupper(u8_rune_t r);

u8_rune_t u8_tolower(u8_rune_t r);

u8_rune_t u8_totitle(u8_rune_t r);

bool u8_isupper(u8_rune_t r);

bool u8_islower(u8_rune_t r);

bool u8_istitle(u8_rune_t r);

bool u8_isalpha(u8_rune_t r);

bool u8_isdigit(u8_rune_t r);

bool u8_isideographic(u8_rune_t r);

bool u8_isspace(u8_rune_t r);
  
#if defined(__cplusplus)
} // extern "C"
#endif // defined(__cplusplus)

#endif /* dasky_utf_capi_h */
